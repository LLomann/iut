def distance(u, v):
	"""
	Entrée: Deux tableaux
	Sortie: Un entier
	Action: Calcule la distance entre deux vecteurs représentés par des tableaux
	"""
	tab = []
	dist = 0
	for i in range(7):
		tab = tab + [(u[i] + v[i]) % 2 ]
	for i in range(7):
		if tab[i] == 1:
			dist += 1
	return dist

def transmission(tab_crypt):
	"""
	Entrée: Un tableau
	Sortie: Un tableau de tableau de tableau
	Action: Convertie les entier du tableau initial en binaire puis renvois leur encodage sur 7 bits représentés par 	un tableau.
	"""
	tab_crypt_str = []
	for i in tab_crypt:
		tab_crypt_str = tab_crypt_str + [str(i)]
	for j in range(len(tab_crypt_str)):
		temp = []
		for k in range(len(tab_crypt_str[j])):
			temp = temp + ['{0:04b}'.format(int(tab_crypt_str[j][k]))]
		for l in range(len(temp)): 
			temp[l] = [(int(temp[l][0]) + int(temp[l][1]) + int(temp[l][3]))%2, (int(temp[l][0]) + int(temp[l][2]) + int(temp[l][3]))%2, int(temp[l][0]), (int(temp[l][1]) + int(temp[l][2]) + int(temp[l][3]))%2, int(temp[l][1]), int(temp[l][2]), int(temp[l][3])]
		tab_crypt_str[j] = temp
	return tab_crypt_str

"""
On vas tester pour tout x vecteur image, la distance entre x et y avec x != y 
Si la distance entre deux vecteurs est < à 3, on affiche un message d'erreur.
"""
tab_reference = transmission([0,1,2,3,4,5,6,7,8,9])

for i in range(len(tab_reference)):
	for j in range(len(tab_reference)):
		if tab_reference[j][0] != tab_reference[i][0]:
			if distance(tab_reference[i][0], tab_reference[j][0]) < 3 :
				print("la distance entre les vecteurs :", tab_reference[i][0]," et :", tab_reference[j][0], " est < à 3.")
