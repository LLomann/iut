import random

def list_prime(n):
  """
  Entrée: un entier
  Sortie: lst, une liste
  Action:  affiche tous les nombres premier compris 100 et l'entier
  """
  lst = []
  for i in range(2, n + 1):
    premier = True
    for j in range(2, i):
      if i % j == 0:
        premier = False
    if premier:
      lst.append(i)
  return lst


def extended_gcd(a, b):
  """
  Entrée: a et b deux entiers et msg 
  Sortie: ua et va deux entiers relatif
  Action: calcul et renvoie d, u et v tel que au + bv = d avec de le pgcd de a et b
  """
  (ua, va, ra) = (1, 0, a)
  (ub, vb, rb) = (0, 1, b)
  while rb != 0:
    quotient = ra // rb
    (ra, ua, va, rb, ub, vb) = (rb, ub, vb, ra - quotient * rb,  ua - quotient * ub, va - quotient * vb)
  return ra, ua, va


def key_creation():
  """
  Entrée: Pas d'entrée
  Sortie: Retourne 3 entiers
  Action: Les 3 sorties représentent respectivement n, la clé publique et la clé privée de chiffrement
  """

  #Calcul de n (le produit de deux nombres premiers)
  n = 0
  while n < 10000 :
    lst = list_prime(1000)
    p = random.choice(lst)
    q = random.choice(lst)
    n = p * q
  
  #Calcul de l'exposant de chiffrement tel que pgcd exposant_chiffrement et indicatrice_euleur sont premiers entre eux
  indicatrice_euleur = (p - 1) * (q - 1)
  exposant_chiffrement = 2
  pgcd, _, _ = extended_gcd(indicatrice_euleur, exposant_chiffrement)
  while pgcd != 1:
      exposant_chiffrement += 1
      pgcd, _, _ = extended_gcd(indicatrice_euleur, exposant_chiffrement)

  #Calcul de l'exposant de déchiffrement "d" inverse de exposant_chiffrement modulo indicatrice_euleur
  d = indicatrice_euleur - 1
  while d > 2 and (d * exposant_chiffrement) % indicatrice_euleur != 1:
      d -= 1

  print("p et q :", p,q)
  print("n :", n)
  print("clef public :", exposant_chiffrement)
  print("clef privé :", d)
  

  return n, exposant_chiffrement, d


def encryption(n, pub, msg):
  """
  Entrée: (n, pub) un entier et msg un entier (le string convertit par la fonction convert_message)
  Sortie: retourne une liste de nombres.
  Action: transforme le msg d'entier en un message chifrée ( une suite de nombres. )
  """

  # le msg qui est un entier naturel doit être strictement < à n
  if msg < n:
    c = (msg**pub) % n
    return c
  else:
    print("le message est trop grand pour être codé")


def decryption(n, priv, msg_chif):
  """
  Entrée: (n, pub) un entier et msg un entier (le message crypté par la fonction encryption)
  Sortie: retourne le message non chiffré.
  Action: transforme le msg_chif en l'ancien massage ( msg ).
  """

  # le msg qui est un entier naturel doit être strictement < à n
  return (msg_chif**priv) % n


def convert_message(msg):
  """
  Entrée: un message en string
  Sortie: retourne une liste de caractère(représentant des nombres.)
  Action: chaque caractère est traduit par une suite de trois chiffre.
  """
  #On code chaque caractère en ASCII sur 3 bits
  lst = []
  for i in range(len(msg)):
    lst.append("000")
    if ord(msg[i]) < 10:
      lst[i][2] = str(ord(msg[i]))
    elif ord(msg[i]) < 100:
      lst[i] = "0"+str(ord(msg[i]))
    else:
      lst[i] = str(ord(msg[i]))

  #On crée une chaine de caractère qui contient tout les caractères précédents mis bout-à-bout
  cara = ""
  for i in lst:
    cara += i
    
  #On divise notre chaine de caractère en paquets de 4
  if len(cara)%4 != 0:
    cara = "0" * (4-(len(cara)%4)) + cara
  lst2=[cara[0:4]]
  for i in range(4,int(len(cara)),4):
    lst2 = lst2 + [cara[i:i+4]]
  return lst2



def deconvert(message_conv):
  """
  Entrée: une liste d'entiers représentant le message codé 
  Sortie: retourne une chaine de caractère
  Action: chaque entier est traduit par un caractère
  """
  #
  lst = []
  print("message converti :", message_conv)
  for i in range(len(message_conv)):
    lst.append("")
    if message_conv[i] < 10:
      lst[i] = "000"+str(message_conv[i])
    elif message_conv[i] < 100:
      lst[i] = "00"+str(message_conv[i])
    elif message_conv[i] < 1000:
        lst[i] = "0"+str(message_conv[i])
    else:
      lst[i] = str(message_conv[i])
  chaine = ""
  for i in lst:
    chaine = chaine + i
  
  lst2 = []
  while len(chaine)%3 != 0:
    chaine = "0" + chaine
  for i in range(len(chaine)//3):
    part = ""
    i = len(chaine)//3 -i -1
    for j in range(3):
      j = 2 - j
      part = chaine[j + i*3] + part
    lst2 = [part] + lst2

  while lst2[0] == "000":
    lst2 = lst2[1:]

  final = ""
  for i in lst2:
    final = final + chr(int(i))

  return final


def RSA(texte):
  """
  Entrée: Une chaine de caractère.
  Sortie: Une chaine de caractère.
  Action: Applique le chiffrement RSA a la chaine de caractère en entrée et affiche l'état du texte à chaque étape.
  """
  clef = key_creation()
  N, PUB, PRIV = clef
  convert = convert_message(texte)
  print("le message converti en suite de nombres vaut :", convert)
  liste_cryptee = [encryption(N, PUB, int(i)) for i in convert]
  print("le message chiffré vaut :", liste_cryptee)
  decrypte = [decryption(N, PRIV, i) for i in liste_cryptee]
  print("le message déchiffré vaut :", decrypte)
  deconv = deconvert(decrypte)
  print("le message final vaut :", deconv)

  if deconv == texte:
    print("le message final est le même que celui de départ")
  else:
    print("le message final est différent de celui de départ, le message de départ vaut :", texte , "et le message  en sortie vaut :", deconv)
  
  return deconv

print(RSA("Jeff est une licorne très Pipou."))