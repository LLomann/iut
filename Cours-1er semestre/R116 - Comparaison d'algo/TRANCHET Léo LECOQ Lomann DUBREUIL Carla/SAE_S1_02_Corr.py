import numpy as np


def noise(vect_msg):
  """
  prend un vecteur vect_msg et renvoie ce vecteur potentiellement bruite
  """
  ### on fait une copie du vecteur initial
  vect = vect_msg.copy()
  ### une chance sur quatre de ne pas bruiter le vecteur
  test = np.random.randint(0,4)
  if test>0:
    index = np.random.randint(0,np.size(vect))
    vect[index] = (vect[index]+1)%2
  return vect
  

def distance(u, v):
  """
  Entrée: Deux tableaux
  Sortie: Un entier
  Action: Calcule la distance entre deux vecteurs représenter par des tableaux
  """
  tab = []
  dist = 0
  for i in range(7):
    tab = tab + [(u[i] + v[i]) % 2 ]
  for i in range(7):
    if tab[i] == 1:
     dist += 1
  return dist


def transmission(tab_crypt):
  """
  Entrée: Un tableau
  Sortie: Un tableau de tableau de tableau
  Action: Convertie les entier du tableau initial en binaire puis renvois leur encodage sur 7 bits représenter par un tableau.
  """
  tab_crypt_str = []
  for i in tab_crypt:
    tab_crypt_str = tab_crypt_str + [str(i)]
  for j in range(len(tab_crypt_str)):
    temp = []
    for k in range(len(tab_crypt_str[j])):
      temp = temp + ['{0:04b}'.format(int(tab_crypt_str[j][k]))]
    for l in range(len(temp)):
      temp[l] = [(int(temp[l][0]) + int(temp[l][1]) + int(temp[l][3]))%2, (int(temp[l][0]) + int(temp[l][2]) + int(temp[l][3]))%2, int(temp[l][0]), (int(temp[l][1]) + int(temp[l][2]) + int(temp[l][3]))%2, int(temp[l][1]),  int(temp[l][2]), int(temp[l][3])]
    tab_crypt_str[j] = temp
      
  return tab_crypt_str


def bruitage(tab_a_transmettre):
  """
  Entrée: Un tableau de tableau de tableau.
  Sortie: Un tableau de tableau de tableau.
  Action: modifier aléatoirement les valeur du tableau en entrée pour simuler des interférences de transmission.
  """
    
  for i in range(len(tab_a_transmettre)):
    for j in range(len(tab_a_transmettre[i])):
      tab_a_transmettre[i][j] = noise(tab_a_transmettre[i][j])
  return tab_a_transmettre


def debruitage(tab_transmis):
  """
  Entrée: Un tableau de tableau de tableau.
  Sortie: Un tableau de tableau de tableau.
  Action: Corrige les valeur du tableau en entrée dans le cas où elles ont été altérées
  """
  
  tab_reference = transmission([0,1,2,3,4,5,6,7,8,9])
  for i in range(len(tab_reference)):
    tab_reference[i] = tab_reference[i][0]
  """print("tab-reference :", tab_reference)"""
  for i in range(len(tab_transmis)):
    for j in range(len(tab_transmis[i])):
      tab_distance = []
      for k in range(len(tab_reference)):
        tab_distance = tab_distance + [distance(tab_transmis[i][j], tab_reference[k])]
      minimum = 0
      """print("tab_distance :", tab_distance)"""
      for l in range(len(tab_distance) -1 ):
        if tab_distance[l+1] < tab_distance[minimum]:
          minimum = l+1 
      for m in range(len(tab_transmis[i][j])):
        tab_transmis[i][j][m] = tab_reference[minimum][m]
  return tab_transmis


def reception(tab_recus):
  """
  Entrée: Un tableau de tableau de tableau.
  Sortie: Un tableau
  Action: Convertis l'entrée en tableau d'entier 
  """
  tab_recus = debruitage(tab_recus)
  for i in range(len(tab_recus)):
    for j in range(len(tab_recus[i])):
      tab_recus[i][j] = str(tab_recus[i][j][2]) + str(tab_recus[i][j][4]) + str(tab_recus[i][j][5]) + str(tab_recus[i][j][6])
      """print(tab_recus[i][j])"""
      tab_recus[i][j] = int(tab_recus[i][j], 2)
      tab_recus[i][j] = tab_recus[i][j] *(10 **(len(tab_recus[i]) - (j + 1)))
    tab_recus[i] = sum(tab_recus[i])
  return tab_recus

def Corr(tab):
  """
  Entrée: Un tableau.
  Sortie: Un tableau.
  Action: Applique les fonction de transmission et de bruitage au tableau.
  """
  print("le tableau initial vaut :", tab)
  transmit = transmission(tab)
  print("le message transmis en binaire sera de la forme :", transmit)
  transmit_bruitee = bruitage(transmit)
  print("exemple du message en binaire une fois bruité ", transmit_bruitee)
  transmit_debruitee = debruitage(transmit_bruitee)
  print("le message débruité en binaire sera de la forme :", transmit_debruitee)
  recept = reception(transmit_debruitee)
  print("le message reçu sera de la forme :", transmit_debruitee, "On obtiens le tableau initial.")
  return recept

Corr([1,2,3,4,5,6])