import random
import numpy as np
import time

#PARTIE 1: CRYPTOGRAPHIE

def list_prime(n):
  """
  Entrée: un entier
  Sortie: lst, une liste
  Action:  affiche tous les nombres premier compris 2 et l'entier
  """
  lst = []
  for i in range(2, n + 1):
    premier = True
    for j in range(2, i):
      if i % j == 0:
        premier = False
    if premier:
      lst.append(i)
  return lst


def extended_gcd(a, b):
  """
  Entrée: a et b deux entiers et msg 
  Sortie: ua et va deux entiers relatif
  Action: calcul et renvoie d, u et v tel que au + bv = d avec de le pgcd de a et b
  """
  (ua, va, ra) = (1, 0, a)
  (ub, vb, rb) = (0, 1, b)
  while rb != 0:
    quotient = ra // rb
    (ra, ua, va, rb, ub, vb) = (rb, ub, vb, ra - quotient * rb,  ua - quotient * ub, va - quotient * vb)
  return ra, ua, va


def key_creation():
  """
  Entrée: Pas d'entrée
  Sortie: Retourne 3 entiers
  Action: Les 3 sorties représentent respectivement n, la clé publique et la clé privée de chiffrement
  """

  #Calcul de n (le produit de deux nombres premiers)
  n = 0
  while n < 10000 :
    lst = list_prime(1000)
    p = random.choice(lst)
    q = random.choice(lst)
    n = p * q
  
  #Calcul de l'exposant de chiffrement tel que pgcd exposant_chiffrement et indicatrice_euleur sont premiers entre eux
  indicatrice_euleur = (p - 1) * (q - 1)
  exposant_chiffrement = 2
  pgcd, _, _ = extended_gcd(indicatrice_euleur, exposant_chiffrement)
  while pgcd != 1:
      exposant_chiffrement += 1
      pgcd, _, _ = extended_gcd(indicatrice_euleur, exposant_chiffrement)

  #Calcul de l'exposant de déchiffrement "d" inverse de exposant_chiffrement modulo indicatrice_euleur
  d = indicatrice_euleur - 1
  while d > 2 and (d * exposant_chiffrement) % indicatrice_euleur != 1:
      d -= 1

  print("p et q :", p,q)
  print("n :", n)
  print("clef public :", exposant_chiffrement)
  print("clef privé :", d)
  

  return n, exposant_chiffrement, d


def encryption(n, pub, msg):
  """
  Entrée: (n, pub) un entier et msg un entier (le string convertit par la fonction convert_message)
  Sortie: retourne une liste de nombres.
  Action: transforme le msg d'entier en un message chifrée ( une suite de nombres. )
  """

  # le msg qui est un entier naturel doit être strictement < à n
  if msg < n:
    c = (msg**pub) % n
    return c
  else:
    print("le message est trop grand pour être codé")


def decryption(n, priv, msg_chif):
  """
  Entrée: (n, pub) un entier et msg un entier (le message crypté par la fonction encryption)
  Sortie: retourne le message non chiffré.
  Action: transforme le msg_chif en l'ancien massage ( msg ).
  """
  
  return (msg_chif**priv) % n


def convert_message(msg):
  """
  Entrée: un message en string
  Sortie: retourne une liste de caractère(représentant des nombres.)
  Action: chaque caractère est traduit par une suite de trois chiffre.
  """
  #On code chaque caractère en ASCII sur 3 bits
  lst = []
  for i in range(len(msg)):
    lst.append("000")
    if ord(msg[i]) < 10:
      lst[i][2] = str(ord(msg[i]))
    elif ord(msg[i]) < 100:
      lst[i] = "0"+str(ord(msg[i]))
    else:
      lst[i] = str(ord(msg[i]))

  #On crée une chaine de caractère qui contient tout les caractères précédents mis bout-à-bout
  cara = ""
  for i in lst:
    cara += i
    
  #On divise notre chaine de caractère en paquets de 4
  if len(cara)%4 != 0:
    cara = "0" * (4-(len(cara)%4)) + cara
  lst2=[cara[0:4]]
  for i in range(4,int(len(cara)),4):
    lst2 = lst2 + [cara[i:i+4]]
  return lst2


def deconvert(message_conv):
  """
  Entrée: une liste d'entiers représentant le message codé 
  Sortie: retourne une chaine de caractère
  Action: chaque entier est traduit par un caractère
  """
  
  lst = []
  print("message converti :", message_conv)
  for i in range(len(message_conv)):
    lst.append("")
    if message_conv[i] < 10:
      lst[i] = "000"+str(message_conv[i])
    elif message_conv[i] < 100:
      lst[i] = "00"+str(message_conv[i])
    elif message_conv[i] < 1000:
        lst[i] = "0"+str(message_conv[i])
    else:
      lst[i] = str(message_conv[i])
  chaine = ""
  for i in lst:
    chaine = chaine + i
  
  lst2 = []
  while len(chaine)%3 != 0:
    chaine = "0" + chaine
  for i in range(len(chaine)//3):
    part = ""
    i = len(chaine)//3 -i -1
    for j in range(3):
      j = 2 - j
      part = chaine[j + i*3] + part
    lst2 = [part] + lst2

  while lst2[0] == "000":
    lst2 = lst2[1:]

  final = ""
  for i in lst2:
    final = final + chr(int(i))

  return final


def RSA(texte):
  """
  Entrée: Une chaine de caractère.
  Sortie: Une chaine de caractère.
  Action: Applique le chiffrement RSA a la chaine de caractère en entrée et affiche l'état du texte à chaque étape.
  """
  clef = key_creation()
  N, PUB, PRIV = clef
  convert = convert_message(texte)
  print("le message converti en suite de nombres vaut :", convert)
  liste_cryptee = [encryption(N, PUB, int(i)) for i in convert]
  print("le message chiffré vaut :", liste_cryptee)
  decrypte = [decryption(N, PRIV, i) for i in liste_cryptee]
  print("le message déchiffré vaut :", decrypte)
  deconv = deconvert(decrypte)
  print("le message final vaut :", deconv)
  return deconv

#PARTIE 2: CODE CORRECTEUR

def noise(vect_msg):
  """
  prend un vecteur vect_msg et renvoie ce vecteur potentiellement bruite
  """
  ### on fait une copie du vecteur initial
  vect = vect_msg.copy()
  ### une chance sur quatre de ne pas bruiter le vecteur
  test = np.random.randint(0,4)
  if test>0:
    index = np.random.randint(0,np.size(vect))
    vect[index] = (vect[index]+1)%2
  return vect
  

def distance(u, v):
  """
  Entrée: Deux tableaux
  Sortie: Un entier
  Action: Calcule la distance entre deux vecteurs représenter par des tableaux
  """
  tab = []
  dist = 0
  for i in range(7):
    tab = tab + [(u[i] + v[i]) % 2 ]
  for i in range(7):
    if tab[i] == 1:
     dist += 1
  return dist


def transmission(tab_crypt):
  """
  Entrée: Un tableau
  Sortie: Un tableau de tableau de tableau
  Action: Convertie les entier du tableau initial en binaire puis renvois leur encodage sur 7 bits représenter par un tableau.
  """
  tab_crypt_str = []
  for i in tab_crypt:
    tab_crypt_str = tab_crypt_str + [str(i)]
  for j in range(len(tab_crypt_str)):
    temp = []
    for k in range(len(tab_crypt_str[j])):
      temp = temp + ['{0:04b}'.format(int(tab_crypt_str[j][k]))]
    for l in range(len(temp)):
      temp[l] = [(int(temp[l][0]) + int(temp[l][1]) + int(temp[l][3]))%2, (int(temp[l][0]) + int(temp[l][2]) + int(temp[l][3]))%2, int(temp[l][0]), (int(temp[l][1]) + int(temp[l][2]) + int(temp[l][3]))%2, int(temp[l][1]),  int(temp[l][2]), int(temp[l][3])]
    tab_crypt_str[j] = temp
      
  return tab_crypt_str


def bruitage(tab_a_transmettre):
  """
  Entrée: Un tableau de tableau de tableau.
  Sortie: Un tableau de tableau de tableau.
  Action: modifier aléatoirement les valeur du tableau en entrée pour simuler des interférences de transmission.
  """
    
  for i in range(len(tab_a_transmettre)):
    for j in range(len(tab_a_transmettre[i])):
      tab_a_transmettre[i][j] = noise(tab_a_transmettre[i][j])
  return tab_a_transmettre


def debruitage(tab_transmis):
  """
  Entrée: Un tableau de tableau de tableau.
  Sortie: Un tableau de tableau de tableau.
  Action: Corrige les valeur du tableau en entrée dans le cas où elles ont été altérées
  """
  
  tab_reference = transmission([0,1,2,3,4,5,6,7,8,9])
  for i in range(len(tab_reference)):
    tab_reference[i] = tab_reference[i][0]
  """print("tab-reference :", tab_reference)"""
  for i in range(len(tab_transmis)):
    for j in range(len(tab_transmis[i])):
      tab_distance = []
      for k in range(len(tab_reference)):
        tab_distance = tab_distance + [distance(tab_transmis[i][j], tab_reference[k])]
      minimum = 0
      """print("tab_distance :", tab_distance)"""
      for l in range(len(tab_distance) -1 ):
        if tab_distance[l+1] < tab_distance[minimum]:
          minimum = l+1 
      for m in range(len(tab_transmis[i][j])):
        tab_transmis[i][j][m] = tab_reference[minimum][m]
  return tab_transmis


def reception(tab_recus):
  """
  Entrée: Un tableau de tableau de tableau.
  Sortie: Un tableau
  Action: Convertis l'entrée en tableau d'entier 
  """
  tab_recus = debruitage(tab_recus)
  for i in range(len(tab_recus)):
    for j in range(len(tab_recus[i])):
      tab_recus[i][j] = str(tab_recus[i][j][2]) + str(tab_recus[i][j][4]) + str(tab_recus[i][j][5]) + str(tab_recus[i][j][6])
      """print(tab_recus[i][j])"""
      tab_recus[i][j] = int(tab_recus[i][j], 2)
      tab_recus[i][j] = tab_recus[i][j] *(10 **(len(tab_recus[i]) - (j + 1)))
    tab_recus[i] = sum(tab_recus[i])
  return tab_recus

def Corr(tab):
  """
  Entrée: Un tableau.
  Sortie: Un tableau.
  Action: Applique les fonction de transmission et de bruitage au tableau.
  """
  print("le tableau initial vaut :", tab)
  transmit = transmission(tab)
  print("le message transmis sera de la forme :", transmit)
  transmit_bruitee = bruitage(transmit)
  print("exemple du message une fois bruité :", transmit_bruitee)
  transmit_debruitee = debruitage(transmit_bruitee)
  print("le message débruité sera de la forme :", transmit_debruitee)
  recept = reception(transmit_debruitee)
  print("le message reçu sera de la forme :", transmit_debruitee, "On obtiens le tableau initial.")
  return recept

def Bilan(texte):
  """
  Entrée: Une chaine de caractère.
  Sortie: Une chaine de caractère.
  Action: Applique la synthèse des fonction des parties Cryptographie et Code correcteur.
  """
  print("le message initial vaut :", texte)
  clef = key_creation()
  N, PUB, PRIV = clef
  convert = convert_message(texte)
  print("le message converti en suite de nombres vaut :", convert)
  liste_cryptee = [encryption(N, PUB, int(i)) for i in convert]
  print("le message chiffré vaut :", liste_cryptee)

  transmit = transmission(liste_cryptee)
  print("le message transmis en binaire sera de la forme :", transmit)
  transmit_bruitee = bruitage(transmit)
  print("exemple du message en binaire une fois bruité :", transmit_bruitee)
  transmit_debruitee = debruitage(transmit_bruitee)
  print("le message débruité en binaire sera de la forme :", transmit_debruitee)
  recept = reception(transmit_debruitee)
  print("le message reçu sera de la forme :", transmit_debruitee, "On obtiens le tableau initial.")

  decrypte = [decryption(N, PRIV, i) for i in recept]
  print("le message déchifré vaut :", decrypte)
  deconv = deconvert(decrypte)
  print("le message final vaut :", deconv)
  
  if deconv == texte:
    print("le message final est le même que celui de départ")
  else:
    print("le message final est différent de celui de départ, le message de départ vaut :", texte , "et le message  en sortie vaut :", deconv)

  return deconv






#BONUS : TESTS
print("Lancement des tests...")
time.sleep(3)
Bilan("Spider-Man est sur l'écran.")
for i in range(5):
  print()
print("_._._._._._")

print("Le test suivant vas se lancer dans 5 secondes ...")
time.sleep(5)
Bilan("€ n'est pas représentable en ascii")
for i in range(5):
  print()
print("_._._._._._")

print("Le test suivant vas sera un test interactif ...")
time.sleep(4)
#Insérer la phrase de votre choix :
test_perso = input("Entrez un message à coder : ")
Bilan(test_perso)
for i in range(5):
  print()
print("_._._._._._")
print()
print("fin des test ...")
print("Merci de votre attention et joyeux Noël et/ou bonne année🎄.")