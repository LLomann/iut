from math import*

def polynome_second_degrés(a,b,c):
    if a == 0:
        return False
    
    delta = (b**2) - (4*(a)*(c))
    if delta > 0:
        x1 = ((-b)-sqrt(delta))/(2*a)
        x2 = ((-b)+sqrt(delta))/(2*a)
        return True, x1, x2
    elif delta == 0:
        return True, (-b)/(2*a)
    elif delta < 0:
        return False

print(polynome_second_degrés(1,3,0))

# def polynome_troisème_degrés(a,b,c,d):#

#exercice du 30/11/2021, suite TP1#

def degres_du_polynome(a):

    return len(a)-1

print(degres_du_polynome([1,2,3,4,5]))

def somme(P,Q):

    for i in range (len(P)):
