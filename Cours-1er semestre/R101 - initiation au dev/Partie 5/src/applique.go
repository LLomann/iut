package main

import "log"

func applique(t []int, f func(x int) int) {
	for i := 0; i < len(t); i++ {
		t[i] = f(t[i])
	}
}

func repli(t []int, f func(x int, y int) int) int {
	somme := t[0]
	for i := 0; i < len(t); i++ {
		somme = f(somme, t[i])
	}
	return somme
}

func main() {

	var t []int = []int{1, 3, 2, 0, -3, 27, 5}
	var t2 []int = []int{1, 3, 2, 0, -3, 27, 5}
	log.Print(t)

	log.Print(repli(t2, func(x, y int) int { return x + y }))

	applique(t, func(x int) int { return 2 * x })
	log.Print(t)
	applique(t2, func(x int) int { return 2 * x })
	log.Print(t2)

	log.Print(repli(t2, func(x, y int) int { return x + y }))

}
