package main

import "fmt"

type listeDC struct {

	valeur int
	next *listeDC
	previous *listeDC

}

func head(l *liste) (int) {

	return l.valeur
	
}

func tail(l *listeDC) (*listeDC) {

	if l.next != nil {
		return l.next
	}
	return nil

}

func append(l *listeDC, v int) (*listeDC){ 

	var nextHeart = listeDC{val: v, next: l, previous: nil}
	if l != nil {
	l.previous = &nextHeart
	}
	return &nextHeart

}

func is_empty(l *liste) (bool) {

	return l == nil

}

