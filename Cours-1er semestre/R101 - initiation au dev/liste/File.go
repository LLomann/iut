package main 

import "fmt"

type filibert struct {

	valeur int
	next *filibert
	q *filibert

}

func pouches(v int, f *filibert) {

	var albert = *filibert{ valeur: v,  next: f.q, q: nil}
	f.q = &albert

}

func poulet(f *filibert) (*filibert) {

	filibert_bis := f.next
	filibert_bis.q = f.q
	return filibert_bis

}

func is_empty(f *filibert_bis) (bool) {

	return f == nil
	
}