package main

import "fmt"

// entier ne peut pas être nil //

type liste struct {

	val int
	next *liste
}

func append(l *liste, v int) (*liste) { 

	var li liste=liste{val:v, next : l}
	return (&li)
}

func tail(l *liste)(*liste){

	return l.next
} 

func head(l *liste) (int) {

	return l.val
}

func recherche(v int, l *liste) (bool) {

	for l!=nil {
		if  l.val == v {
			return true
		}
		l=l.next
	}
	return false
}

func delete(v int, l *liste) (*liste) {

	var li *liste = l
	var prec *liste = l

	if l.val== v {
		return l.next
	}
	for l!= nil {
		if l.val != v {
			prec = l

		}else{
			prec.next =l.next
			break
		}		
		l=l.next	
	}
	return li
}

func afficher(l *liste) {

	for l!=nil {
		fmt.Println(l.val)
		l=l.next
	}
}

func insertion(v int, ll *liste) (*liste) {
	
}

func tri(l *liste) (*liste){

	for l!=nil {
		ll = insertion(head(l),ll)
		l = l.next
	}

}

func main() {	
	
	var l  *liste=nil
	l=append(l,3)
	l=append(l,5)
	l=append(l,7)
	l=append(l,9)

	var x int = head(l)
	fmt.Println(" tête de liste :", x)

	fmt.Println(recherche(6,l))

	fmt.Println("afficher:")
	afficher(l)

	fmt.Println(delete(3,l))

	fmt.Println("afficher:")
	afficher(l)

}