package tri

/*
La fonction tri doit trier un tableau d'entiers du plus petit au plus grand.
Cette fonction ne doit pas modifier le tableau donné en entrée.

# Entrée
- tinit : un tableau d'entiers qui ne doit pas être modifié.

# Sortie
- tfin : un tableau contenant les mêmes entiers que tinit mais triés du plus
         petit au plus grand.
*/

func tri(tinit []int) (tfin []int) {

	for _,v := range tinit {
		for i :=0; i < len(tfin); i++ {
			if v < tfin[i] {
				tfin[i], v = v, tfin[i]
			}
		}
		tfin = append(tfin,v)
	}
	return tfin
}


	// if tinit == nil {
	// 	return []int{}
	// }

	// tfin = []int{}

	// for i:=0; i < len(tinit); i++ {
	// 	tfin.append(tfin, tinit[i])
	// }

	// for k:=0; k < len(tfin); k++ {
	// 	for j:=0; j < len(tfin)-1; j++ {
	// 		if tfin[k] > tfin[j] {
	// 			tfin[k] = tfin[j]
	// 			tfin[j] = tfin[k]
	// 		}
	// 	}
	// }

	// return tfin