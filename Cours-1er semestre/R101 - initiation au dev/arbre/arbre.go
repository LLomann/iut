package main

import "fmt"

type arbre struct {
	valeur int
	fg *arbre
	fd *arbre
}

type sapin struct {
	racine int
	bd *arbre
	bg *arbre
	precedent *arbre
}

var racine *arbre = nil

func make(v int, tbo *arbre, sapin *arbre) (*arbre) {
	var tree *arbre = &arbre{v, tbo, sapin }
	return tree
}


func afficher(arbre *arbre){

	if arbre == nil {
		fmt.Println(0)
	}

	if arbre.fg != nil {
		afficher(arbre.fg)
	}

	fmt.Println(arbre.valeur)

	if arbre.fd != nil {
		afficher(arbre.fd)
	}
}

func afficher_gauche(arbre *arbre){

	if arbre == nil {
		fmt.Println(0)
	}

	if arbre.fg != nil {
		afficher(arbre.fg)
	}

	fmt.Println(arbre.valeur)

}

func afficher_droit(arbre *arbre){

	if arbre == nil {
		fmt.Println(0)
	}

	fmt.Println(arbre.valeur)

	if arbre.fd != nil {
		afficher(arbre.fd)
	}
}

func isEmpty(arbre *arbre) (bool) {

	return arbre == nil

}

func feuille(tree *arbre) bool {
	return tree.fd == nil && tree.fg == nil
}

func inserer(v int, racine *arbre){

	if isEmpty(racine) {
		make(v, nil, nil)
	}

	if v < racine.valeur {
		if isEmpty(racine.fg) {
			racine.fg = make(v, nil, nil )
		}else{
			inserer(v, racine.fg)

		}
	}else{
		if isEmpty(racine.fd) {
			racine.fd = make(v ,nil, nil)
		}else{
			inserer(v, racine.fd)
		}
	}
}

func rechercher(v int, tree *arbre) bool{

	if tree == nil {
		return false
	}	

	if v == tree.valeur {
		return true
	}

	if v > tree.valeur {
		return rechercher(v, tree.fd)
	}

	if v < tree.valeur {
		return rechercher(v, tree.fg)
	}

	return false
	
}

func main() {

	var tree *arbre = make(10, nil, nil)

	inserer(6, tree)
	inserer(11, tree)
	inserer(3, tree)
	inserer(19, tree)
	afficher_gauche(tree)
	fmt.Println(rechercher(20, tree))
	
}
