package souschaine

/*
Une chaine sc est une sous chaine d'une chaine s si on peut 
reconstruire s en rajoutant des lettres à sc. On peut 
le voir de manière récursive : sc = asc' 
 
(avec a en caractère et sc' une chaine) est une sous 
chaine de s si s = as' et sc' est une sous chaine de s' ou 
si s = bs' et asc' est une sous chaine de s'.

# Entrées
- s : une chaîne
- sc : une chaîne

# Sortie
- b : un booléen indiquant si sc est une sous chaine de s

# Exemple
sousChaine("abcde", "ace") = true
*/
func sousChaine(s string, sc string) (b bool) {

	if len(sc) == 0 {
		return true
	}
	if len(s) == 0 {
		return false
	}

	var bool_chaine bool
	var i_chaine int
	bool_chaine,_ = est_dans_chaine(s, string(sc[0])
	_,i_chaine = est_dans_chaine(s, string(sc[0])

	if bool_chaine {
		chaine = chaine[:i_chaine] + chaine[i_chaine:]
		return sousChaine(chaine, souschaine[1:])
	}
	return false


func est_dans_chaine(s, lettre string) (bool) {
	for i:= 0; i<len(s); i++ {
		if lettre == string(s[i]) {
			return true
		}
	}
	return false
}


