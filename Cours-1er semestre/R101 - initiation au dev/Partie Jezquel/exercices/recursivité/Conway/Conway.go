package main

import "fmt"

func Conway(n int) (u []int ) {

	if n == 0 {
		u = []int{1}
		return  u
	}

	tabs:= Conway(n-1)
	valeurs:= tabs[0]
	compteur:= 1

	for i:=1; i<len(tabs); i++ {
		if tabs[i] == valeurs {
			compteur +=1
		}else{
			u = append(u, compteur)
			u = append(u, valeurs)
			compteur =1
			valeurs = tabs[i]
		}
	}
	return u
}

func main() {

	fmt.Println(Conway(1))

}
