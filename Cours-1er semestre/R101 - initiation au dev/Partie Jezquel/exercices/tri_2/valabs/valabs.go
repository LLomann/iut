package valabs

/*
La fonction valabs doit trier un tableau d'entiers de la plus petite valeur absolue
à la plus grande valeur absolue. En cas
d'égalité de valeur absolue, les nombres
négatifs doivent être placés avant les
nombres positifs.

# Entrée
- tab : un tableau d'entiers
*/
func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func valabs(tablo []int) {
	for i := 1; i < len(tablo); i++ {
		var j int = i
		for j > 0 && abs(tablo[j]) < abs(tablo[j-1]) {
			tablo[j], tablo[j-1] = tablo[j-1], tablo[j]
			j -= 1
		}

	}
}
