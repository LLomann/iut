package alphabetique

/*
La fonction alphabetique trie un tableau de chaînes de caractères dans l'ordre
alphabétique.

# Entrée
- dico : le tableau de chaînes de caractères à trier
*/

func alphabetique(dico []string) {
	for i := 1; i < len(dico); i++ {
		var j int = i
		for j > 0 && dico[j] < dico[j-1] {
			dico[j], dico[j-1] = dico[j-1], dico[j]
			j -= 1
		}

	}
}
