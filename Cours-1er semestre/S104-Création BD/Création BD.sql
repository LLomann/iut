
______________________________________________________________________________________________________________________________________________________________________________________________________________

		8 CREATION DE LA BASE DE DONNEES Transport AVEC ORACLE: 1 Création les différents relations de la base Transport - 2 ajout des contraintes d intégrité.
______________________________________________________________________________________________________________________________________________________________________________________________________________


CREATE TABLE PASSAGER (
numpass NUMBER(2,0) NOT NULL,
nom_pass VARCHAR(50) NOT NULL,
prenom_pass VARCHAR(50) NOT NULL,
age_pass NUMBER(2,0) NOT NULL,
adresse_pass VARCHAR(50) NOT NULL,
numtrajet NUMBER(2,0),
CONSTRAINT cle_passager PRIMARY KEY(numpass)
);

CREATE TABLE CONDUCTEUR (
numcond NUMBER(3,0) NOT NULL,
nom_cond VARCHAR(50) NOT NULL,
adresse_cond VARCHAR(50) NOT NULL,
num_tel NUMBER(10,0) NOT NULL,
date_agr NUMBER(4,0) NOT NULL,
nb_trajet NUMBER(2,0) NOT NULL,
nb_pass NUMBER(3,0) NOT NULL,
numveh NUMBER(3,0),
CONSTRAINT cle_conducteur PRIMARY KEY(numcond)
);

CREATE TABLE VEHICULE (
numveh NUMBER(3,0)NOT NULL,
energie_v VARCHAR(50) NOT NULL,
nb_places_v NUMBER(2,0)NOT NULL,
annee_circ_v NUMBER(4,0)NOT NULL,
marque_v  VARCHAR(50) NOT NULL, 
type_v VARCHAR(50) NOT NULL,
numcond NUMBER(3,0),
CONSTRAINT cle_vehicule PRIMARY KEY(numveh)
);

CREATE TABLE TRAJET (
numtrajet NUMBER(2,0) NOT NULL,
ville_dep VARCHAR(50) NOT NULL,
heure_dep NUMBER(4,2) NOT NULL,
ville_arr VARCHAR(50) NOT NULL,
heure_arr NUMBER(4,2) NOT NULL,
prix NUMBER(4,2) NOT NULL,
numveh NUMBER(3,0),
numcond NUMBER(3,0),
CONSTRAINT cle_trajet PRIMARY KEY(numtrajet),
CONSTRAINT kf_numveh FOREIGN KEY(numveh) REFERENCES VEHICULE(numveh),
CONSTRAINT kf_numcond FOREIGN KEY(numcond) REFERENCES CONDUCTEUR(numcond)
);

CREATE TABLE AVIS (
idavis NUMBER(4,0) NOT NULL,
numpass NUMBER(3,0) NOT NULL,
numcond NUMBER(3,0) NOT NULL,
text_avis LONG NOT NULL,
note NUMBER(2,0) NOT NULL,
CONSTRAINT cle_avis PRIMARY KEY(idavis),
CONSTRAINT kf_numpass_avis FOREIGN KEY(numpass) REFERENCES PASSAGER(numpass),
CONSTRAINT kf_numcond_avis FOREIGN KEY(numcond) REFERENCES CONDUCTEUR(numcond)
);

ALTER TABLE VEHICULE ADD CONSTRAINT kf_numcond_veh FOREIGN KEY(numcond) REFERENCES CONDUCTEUR(numcond);
ALTER TABLE CONDUCTEUR ADD CONSTRAINT kf_numveh_cond FOREIGN KEY(numveh) REFERENCES VEHICULE(numveh);
ALTER TABLE PASSAGER ADD CONSTRAINT kf_numtrajet_pass FOREIGN KEY(numtrajet) REFERENCES TRAJET(numtrajet);

______________________________________________________________________________________________________________________________________________________________________________________________________________

				3) Insertion des tuples dans chacune des relations.
______________________________________________________________________________________________________________________________________________________________________________________________________________


INSERT INTO PASSAGER (numpass, nom_pass, prenom_pass, age_pass, adresse_pass) VALUES (96, 'LECOQ', 'Lexann', 20, 'Redon');
INSERT INTO PASSAGER (numpass, nom_pass, prenom_pass, age_pass, adresse_pass) VALUES (72, 'NUAGE', 'Momo', 18, 'Angers');
INSERT INTO PASSAGER (numpass, nom_pass, prenom_pass, age_pass, adresse_pass) VALUES (23, 'LEBRICOLEUR', 'Bob', 50 , 'Rennes');
INSERT INTO PASSAGER (numpass, nom_pass, prenom_pass, age_pass, adresse_pass) VALUES (17, 'LEMARECHAL', 'Marion', 35, 'Nantes');
INSERT INTO PASSAGER (numpass, nom_pass, prenom_pass, age_pass, adresse_pass) VALUES (48, 'RETIF',  'Elena', 19, 'Lyon');
INSERT INTO PASSAGER (numpass, nom_pass, prenom_pass, age_pass, adresse_pass) VALUES (61, 'JAGER',  'Eren', 26, 'Nantes');
INSERT INTO PASSAGER (numpass, nom_pass, prenom_pass, age_pass, adresse_pass) VALUES (37, 'CROCHE', 'Sarah', 15, 'Paris');
INSERT INTO PASSAGER (numpass, nom_pass, prenom_pass, age_pass, adresse_pass) VALUES (11, 'MINION', 'Sabrina', 35, 'Lille');
INSERT INTO PASSAGER (numpass, nom_pass, prenom_pass, age_pass, adresse_pass) VALUES (26, 'ACKERMAN', 'Levi', 18, 'Brest');
INSERT INTO PASSAGER (numpass, nom_pass, prenom_pass, age_pass, adresse_pass) VALUES (54, 'BOUTDEBOIS', 'Enzo', 35, 'Nantes');
INSERT INTO PASSAGER (numpass, nom_pass, prenom_pass, age_pass, adresse_pass) VALUES (39, 'KANEKI',  'Ken', 19, 'Quimper');
INSERT INTO PASSAGER (numpass, nom_pass, prenom_pass, age_pass, adresse_pass) VALUES (67, 'LEONART',  'Annie', 26, 'Lyon');
INSERT INTO PASSAGER (numpass, nom_pass, prenom_pass, age_pass, adresse_pass) VALUES (75, 'LEGUELEC', 'Camille', 15, 'Angers');
INSERT INTO PASSAGER (numpass, nom_pass, prenom_pass, age_pass, adresse_pass) VALUES (59, 'ARLET', 'Armin', 50 , 'Rennes');



INSERT INTO CONDUCTEUR (numcond, nom_cond, adresse_cond, num_tel, date_agr, nb_trajet, nb_pass) VALUES (69, 'JEFF', 'Baule', 0607080901, 2012, 25, 62);
INSERT INTO CONDUCTEUR (numcond, nom_cond, adresse_cond, num_tel, date_agr, nb_trajet, nb_pass) VALUES (327, 'BOUBOU', 'Angers', 0763145621, 2013, 14, 19);
INSERT INTO CONDUCTEUR (numcond, nom_cond, adresse_cond, num_tel, date_agr, nb_trajet, nb_pass) VALUES (410, 'JEAN', 'Rennes', 0642114875, 2011, 9, 0);
INSERT INTO CONDUCTEUR (numcond, nom_cond, adresse_cond, num_tel, date_agr, nb_trajet, nb_pass) VALUES (754, 'ERWIN', 'Nantes', 0712045787, 1999, 13, 26);
INSERT INTO CONDUCTEUR (numcond, nom_cond, adresse_cond, num_tel, date_agr, nb_trajet, nb_pass) VALUES (87, 'TOKA', 'Vannes', 0732095312, 2020, 21, 32);
INSERT INTO CONDUCTEUR (numcond, nom_cond, adresse_cond, num_tel, date_agr, nb_trajet, nb_pass) VALUES (740, 'CHACHA', 'Quimper', 0697890951, 2019, 10, 16);
INSERT INTO CONDUCTEUR (numcond, nom_cond, adresse_cond, num_tel, date_agr, nb_trajet, nb_pass) VALUES (487, 'LWI', 'Paris', 0621103167, 2014, 9, 15);
INSERT INTO CONDUCTEUR (numcond, nom_cond, adresse_cond, num_tel, date_agr, nb_trajet, nb_pass) VALUES (910, 'BERTOLT', 'Lyon', 0632654953, 2017, 34, 64);
INSERT INTO CONDUCTEUR (numcond, nom_cond, adresse_cond, num_tel, date_agr, nb_trajet, nb_pass) VALUES (354, 'SEBASTIEN', 'Lille', 0610605943, 2018, 7, 11);
INSERT INTO CONDUCTEUR (numcond, nom_cond, adresse_cond, num_tel, date_agr, nb_trajet, nb_pass) VALUES (201, 'HANSI', 'Nantes', 0765021630, 2017, 16, 24);
INSERT INTO CONDUCTEUR (numcond, nom_cond, adresse_cond, num_tel, date_agr, nb_trajet, nb_pass) VALUES (14, 'SEIG', 'Vertou', 06102640513, 2015, 8, 10);



INSERT INTO vehicule VALUES (741, 'ESSENCE', 4, 2016, 'BMW', 'COUPE', 69) ;
INSERT INTO vehicule VALUES (320, 'ESSENCE', 6, 2016, 'LAND ROVER', 'PICK-UP', 87);
INSERT INTO vehicule VALUES (362, 'ESSENCE', 7, 2013, 'VOLKSWAGEN', 'MINIBUS', 14);
INSERT INTO vehicule VALUES (299, 'DIESEL', 5, 2012, 'PEUGEOT', 'BREAK', 327);
INSERT INTO vehicule VALUES (112, 'DIESEL', 4, 2011, 'MERCEDES', 'COUPE', 410);
INSERT INTO vehicule VALUES (54, 'ESSENCE', 5, 1999, 'HYUNDAI', 'HYBRIDE', 754);
INSERT INTO vehicule VALUES (478, 'ELECTRIQUE', 4, 2018, 'RENAULT', 'POLYVALENTE', 740);
INSERT INTO vehicule VALUES (13, 'DIESEL', 5, 2017, 'VOLKSWAGEN', 'SUV', 487);
INSERT INTO vehicule VALUES (578, 'ESSENCE', 5, 2015, 'AUDI', 'COUPE', 910);
INSERT INTO vehicule VALUES (3, 'DIESEL', 5, 2014, 'PEUGEOT', 'CARAVANE', 354);
INSERT INTO vehicule VALUES (915, 'ELECTRIQUE', 4, 2012, 'RENAULT', 'POLYVALENTE', 201);
INSERT INTO vehicule VALUES (66, 'ESSENCE', 2, 1930, 'FORD', 'VIEILLE VOITURE', 14);

UPDATE CONDUCTEUR SET NUMVEH = 741 WHERE NUMCOND = 69;
UPDATE CONDUCTEUR SET NUMVEH = 299 WHERE NUMCOND = 327;
UPDATE CONDUCTEUR SET NUMVEH = 362 WHERE NUMCOND = 410;
UPDATE CONDUCTEUR SET NUMVEH = 54 WHERE NUMCOND = 754;
UPDATE CONDUCTEUR SET NUMVEH = 320 WHERE NUMCOND = 87;
UPDATE CONDUCTEUR SET NUMVEH = 478 WHERE NUMCOND = 740;
UPDATE CONDUCTEUR SET NUMVEH = 13 WHERE NUMCOND = 487;
UPDATE CONDUCTEUR SET NUMVEH = 578 WHERE NUMCOND = 910;
UPDATE CONDUCTEUR SET NUMVEH = 3 WHERE NUMCOND = 354;
UPDATE CONDUCTEUR SET NUMVEH = 915 WHERE NUMCOND = 201;
UPDATE CONDUCTEUR SET NUMVEH = 66 WHERE NUMCOND = 14;


INSERT INTO TRAJET VALUES (5, 'Nantes', (10.57), 'Angers', (12.06), (5.50), 741, 69);
INSERT INTO TRAJET VALUES (6, 'Redon', 13.28, 'Rennes', 14.31, 6.00, 299, 327);
INSERT INTO TRAJET VALUES (15, 'Brest', 11.09, 'Nantes', 14.42, 25.50, 54, 754);
INSERT INTO TRAJET VALUES (7, 'Quimper', 14.23, 'Redon', 16.02, 14.50, 320, 87);
INSERT INTO TRAJET VALUES (22, 'Nantes', 17.53, 'Cholet', 18.58, 5.00, 478, 740);
INSERT INTO TRAJET VALUES (13, 'Brest', 18.01, 'Rennes', 20.49, 28.00, 13, 487);
INSERT INTO TRAJET VALUES (19, 'Lille', 18.50, 'Chessy', 20.35, 20.00, 578, 910);
INSERT INTO TRAJET VALUES (29, 'Paris', 9.45, 'Lille', 12.03, 15.00, 3, 354);
INSERT INTO TRAJET VALUES (17, 'Lille', 7.32, 'Paris', 8.54, 22.00, 741, 69);
INSERT INTO TRAJET VALUES (25, 'Baule', 12.46, 'Rennes', 13.23, 10.00, 299, 327);
INSERT INTO TRAJET VALUES (16, 'Chessy', 10.56, 'Angers', 13.59, 22.00, 915, 201);
INSERT INTO TRAJET VALUES (8, 'Paris', 19.49, 'Redon', 32.30, 34.50, 741, 69);
INSERT INTO TRAJET VALUES (20, 'Angers', 8.09, 'Chessy', 11.46, 24.00, 915,201);
INSERT INTO TRAJET VALUES (32, 'Vertou', 10.08, 'Redon', 11.31, 12.00, 362, 14);
INSERT INTO TRAJET VALUES (3, 'Chessy', 18.43, 'Brest', 23.30, 32.00, 362, 410);
INSERT INTO TRAJET VALUES (2, 'Brest', 13.12, 'Quimper', 14.20, 5.50, 362, 410);
INSERT INTO TRAJET VALUES (27, 'Cholet', 6.13, 'Nantes', 7.09, 6.00, 478, 740);

UPDATE PASSAGER SET numtrajet = 8 WHERE numpass = 96;
UPDATE PASSAGER SET numtrajet = 16 WHERE numpass = 72;
UPDATE PASSAGER SET numtrajet = 27 WHERE numpass = 23;
UPDATE PASSAGER SET numtrajet = 5 WHERE numpass = 17;
UPDATE PASSAGER SET numtrajet = 22 WHERE numpass = 48;
UPDATE PASSAGER SET numtrajet = 32 WHERE numpass = 61;
UPDATE PASSAGER SET numtrajet = 25 WHERE numpass = 37;
UPDATE PASSAGER SET numtrajet = 13 WHERE numpass = 11;
UPDATE PASSAGER SET numtrajet = 6 WHERE numpass = 26;
UPDATE PASSAGER SET numtrajet = 15 WHERE numpass = 54;
UPDATE PASSAGER SET numtrajet = 7 WHERE numpass = 39;
UPDATE PASSAGER SET numtrajet = 2 WHERE numpass = 67;
UPDATE PASSAGER SET numtrajet = 20 WHERE numpass = 75;
UPDATE PASSAGER SET numtrajet = 19 WHERE numpass = 59;


INSERT INTO AVIS VALUES (393, 23, 487, 'PS: le tuning de licorne sur la voiture est à enlever', 7);
INSERT INTO AVIS VALUES (637, 26, 754, 'Trop cool et je ne me suis même pas noyée!', 8);
INSERT INTO AVIS VALUES (451, 17, 410, 'Bravo! Jean est au top! Première expérience Blablacar pour moi et c était tout simplement parfait du début à la fin.', 9);
INSERT INTO AVIS VALUES (2019, 11, 910, 'Parfait Bertolt. PLus, ça serait top.', 8);
INSERT INTO AVIS VALUES (7106, 61, 201, 'Hansi était un très bon conducteur et très souriant.',6);
INSERT INTO AVIS VALUES (513, 39, 14, 'Chauffeur très désagreéable et peu aimable et conduite un peu dangereuse. Je ne recommmande pas.', 2);
INSERT INTO AVIS VALUES (204, 59, 87, 'Si vous n aimez pas la culture, l histoire, le bon temps a cote de quelqu un avec une belle experience de vie. Il ne faut pas aller avec Toka.',10);
INSERT INTO AVIS VALUES (432, 67, 201, 'Hansi était un très bon conducteur mais l odeur de la cigarette était insupportable.',3);

______________________________________________________________________________________________________________________________________________________________________________________________________________

					9 CONSULTATION DES DONNÉES: Requêtes SQL
______________________________________________________________________________________________________________________________________________________________________________________________________________


1) SELECT DISTINCT nom_cond FROM CONDUCTEUR JOIN TRAJET ON trajet.numcond = conducteur.numcond WHERE (ville_dep = 'Nantes' AND ville_arr = 'Cholet') OR (ville_dep = 'Cholet' AND ville_arr = 'Nantes');
2) SELECT energie_v as energie_veh, conducteur.numveh as numero_vehicule, nom_cond FROM VEHICULE JOIN conducteur ON conducteur.numcond = vehicule.numcond WHERE vehicule.annee_circ_v < 2000 AND conducteur.adresse_cond = 'Nantes';
3) SELECT nom_cond FROM CONDUCTEUR INNER JOIN AVIS ON avis.numcond = conducteur.numcond WHERE conducteur.adresse_cond = 'Nantes' GROUP BY conducteur.nom_cond HAVING(MIN(avis.note) > 5)
4) SELECT nom_cond, nb_trajet FROM CONDUCTEUR ORDER BY nb_trajet DESC FETCH FIRST 3 ROWS ONLY;
5) SELECT DISTINCT passager.nom_pass, adresse_pass, ville_arr FROM TRAJET JOIN PASSAGER ON trajet.numtrajet = passager.numtrajet WHERE passager.adresse_pass = trajet.ville_arr;
6) SELECT nom_cond, date_agr FROM CONDUCTEUR WHERE date_agr > 2014;
7) SELECT nom_cond, nb_pass FROM CONDUCTEUR WHERE nb_pass = 0;
8) SELECT nom_cond, nb_pass FROM CONDUCTEUR ORDER BY nb_pass DESC FETCH FIRST 1 ROWS ONLY;


