package particles

import (
	"math/rand"
	"project-particles/config"
)

// NewSystem est une fonction qui initialise un système de particules et le
// retourne à la fonction principale du projet, qui se chargera de l'afficher.
// C'est à vous de développer cette fonction.
// Dans sa version actuelle, cette fonction affiche une particule blanche au
// centre de l'écran.

func NewSystem() System {

	//Récupération des variable présente dans config.json//

	nombre_particule := config.General.InitNumParticles		// 10
	// random := config.General.RandomSpawn					// true ou false

	screen_X := config.General.WindowSizeX	// taille : 800
	screen_Y := config.General.WindowSizeY	// taille : 600

	// Création des variable du tableau de particule et des particule

	var tab_particule[]Particle =[]Particle{}
	var p Particle

	// boucle qui crée le nombre de particule present dans la variable InitNumParticles 
	// et qui lui donne des valeur différente//

	random := config.General.RandomSpawn

	var position_X float64
	var position_Y float64


	for i:=1; i<nombre_particule; i++ {

	if random == true {

		position_X = rand.Float64() * float64(screen_X)
		position_Y = rand.Float64() * float64(screen_Y)

	}else{

		position_X = config.General.SpawnnX
		position_Y = config.General.SpawnnY

	}

		p = Particle{
			PositionX: position_X,
			PositionY: position_Y,
			ScaleX:    1, ScaleY: 1,
			ColorRed: 1, ColorGreen: 1, ColorBlue: 1,
			Opacity: 1,
		}

		tab_particule = append(tab_particule, p)
	}
			
	return (System{Content: tab_particule})

}
