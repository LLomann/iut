# BUT STID - statistique et informatique décisionnelle


Monsieur, Madame
Je suis actuellement en 1er années de B.U.T Informatique à l’IUT de Nantes. Je porte un intérêt particulier en ce qui concerne les mathématiques et l’informatique en général, cependant, j’ai eu quelques difficultés en ce qui concerne le codage, que cela soit sur Python,Golang, Kotlin,Java..
je reste cependant assez intéresser par la construction de base de données (SQL) ou le codage de site internet (HTML/CSS).

Votre formation STID statistique et informatique décisionnelle me paraît donc la mieux adapter à ma reconversion d’étude ; en effet, j’ai quelques bases en informatique, mais j’ai également des facilités en ce qui concerne les mathématiques. Il s’agit d’une matière que j’ai toujours appréciée et dans le qu’elle je me suis toujours sentie à l’aise et dans là qu’elle mes notes ont toujours été plus que correcte.
C’est donc avec beaucoup d’espoir que j’espère être accepté dans votre formation, car je pense que votre enseignement de qualité me permettra d'approfondir mes connaissances dans les domaines d'informatique et des mathématiques, mais également d’établir des bases solides dans le domaine de l’économie ainsi que les autres matières qui englobent votre formation.
Déterminer, consciencieuse et très rigoureuse, je m’engage à me montrer assidue tout au long de votre formation afin d’atteindre mes objectifs professionnels.

En vous remerciant par avance de l’attention que vous avez portée à ma candidature, je vous prie de recevoir, Madame, Monsieur, l’expression de mes salutations distinguées.

LECOQ Lomann



# DNMADE option graphique


Monsieur, Madame
Je suis actuellement en 1er années de B.U.T Informatique à l’IUT de Nantes. Je me suis malheureusement rendu compte que cette formation ne me plaisait pas et je cherche dont a m'a réorienté. Après de nombreuses recherches, la formation DNMADE option graphique me paraît celle la mieux adapté pour ma réorientation.

En effet, lors de mes anciennes recherchées pour trouver d'éventuelles études, les formations DNMADE m’avaient beaucoup intéressé, et ce, dès la seconde ( année durant là qu’elles je me suis rendus aux portes ouvertes pour le DNMADE option graphisme ). Je n’ai malheureusement pas été retenue pour dans cette formation après l’obtention de mon BAC. Cependant, cette formation continue à fortement m’intéresser ; la plupart de mon temps libre est consacré au dessin et aux activités manuelles, et je suis vraiment passionné par tous ce qui touche à l’art en général.
Cela serait pour moi une merveilleuse opportunité que de travailler dans cette formation.

Déterminer, consciencieuse et très rigoureuse, je m’engage à me montrer assidue tout au long de votre formation afin d’atteindre mes objectifs professionnels.

En vous remerciant par avance de l’attention que vous avez portée à ma candidature, je vous prie de recevoir, Madame, Monsieur, l’expression de mes salutations distinguées.

LECOQ Lomann



# DCG : diplôme de comptabilité et de gestion 

Monsieur, Madame
Je suis actuellement en 1er années de B.U.T Informatique à l’IUT de Nantes. Je me suis malheureusement rendu compte que cette formation ne me plaisait pas et je cherche dont a m'a réorienté. Après de nombreuses recherches, la formation BTS Comptabilité et gestion me paraît celle la mieux adapté pour ma réorientation.

{...}

Déterminer, consciencieuse et très rigoureuse, je m’engage à me montrer assidue tout au long de votre formation afin d’atteindre mes objectifs professionnels.

En vous remerciant par avance de l’attention que vous avez portée à ma candidature, je vous prie de recevoir, Madame, Monsieur, l’expression de mes salutations distinguées.

LECOQ Lomann