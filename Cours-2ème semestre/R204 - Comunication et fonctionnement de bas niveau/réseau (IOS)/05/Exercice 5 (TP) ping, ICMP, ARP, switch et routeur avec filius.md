# Filius, simulateur du fonctionnement des réseaux

---
## Installation et lancement

- Télécharger la dernière version _Zip_ de Filius (v1.13 au 07/03/2022) sur https://www.lernsoftware-filius.de/Herunterladen
- Dézipper dans un répertoire
- Puis exécuter le jar

Les lignes suivantes font le job pour vous :

    wget https://www.lernsoftware-filius.de/downloads/Setup/filius-1.13.2.zip 
    mkdir filius-1.13.2 
    unzip filius-1.13.2.zip -d filius-1.13.2 
    cd filius-1.13.2
    java -jar filius.jar 

---
## Mode de conception et mode de simulation

Ouvrir le fichier du projet FIXME

Le réseau affiché vous dit quelque chose ? Question rhétorique qui n'attend pas de réponse...

Filius offre 2 modes : 
- 1. un mode de conception d'un réseau (icone du _marteau_) 
- 2. un mode de simulation du fonctionnement d'un réseau (icone _lecture_).

Dans le présent devoir, vous travaillerez dans le mode simulation (la conception a déjà été faite pour vous). 

Dans ce mode simulation, vous allez faire un certain nombre de manipulations. Si vous souhaitez réinitialiser votre environnement alors passer en mode conception, faire "feuille blanche" puis ouvrir à nouveau le fichier du projet de simulation donné.

---
## Un réseau d'ordinateurs et leurs systèmes pré-configurés

Passer en mode simulateur.

Le projet suivant présente 3 domaines de diffusion interconnectés par un routeur. Les ordinateurs portent le nom de leur IP.

Si vous survolez un ordinateur, une cartouche bleu s'affiche. 

Est-ce que tous les ordinateurs ont bien une adresse IP et une adresse MAC distinctes des autres ? Ne vous occupez pas des autres informations.

Survolez le routeur. Combien d'adresses IP et d'adresses MAC a-t-il ? 

Les switches ne sont pas des équipements que l'on peut adresser. Ils n'ont donc pas besoin d'adresse IP, ni d'adresse MAC. 

Une cartouche s'affiche t'elle quand on les survole ? 

Chaque ordinateur possède un "bureau" qui permet d'installer ou d'exécuter des applications sur celui-ci. Pour accéder au bureau d'un ordinateur, il suffit de faire "click droit > afficher bureau" ou bien simplement "click gauche" au dessus du dit ordinateur. Via le bureau, il y a un "installeur d'applications". Les applications sont ensuite rendues disponibles via le bureau.  

Sur l'ordinateur 192.168.1.1, il y a quelques applications installées. 
Exécutez "Ligne de commande" (en cliquant sur l'icone du même nom sur le bureau). Une fois ouverte, remarquez la petite croix dans l'angle de la fenêtre qui permet de fermer l'invite et regagner le bureau. Vous ne perdrez pas l'historique en faisant cela.

Dans l'invite de commande, la commande `help` permet de connaître les commandes disponibles. Les commandes linux sont _un peu différentes_.

La commande `ipconfig` renseigne sur les paramètres réseaux de l'ordinateur. Exécutez la. Vous devez trouver l'adresse IP et l'adresse MAC.

Les autres informations seront utiles pour l'adressage des paquets IP et leur routage sur l'interconnexion des réseaux. Une commande nous informera sur ces routes (la commande `route`). Mais on verra cela plus tard.

La commande `arp` permet de sonder les adresses MAC connues par l'ordinateur. Ces MAC appartiennent aux ordinateurs du réseau local avec lesquels l'ordinateur courant a communiqué. Exécutez la commande. Une seule ligne est présente dans le cache, l'adresse IP et l'adresse MAC associée ne correspondent pas à des adresses individuelles. On verra plus tard ce dont il s'agit.

Testez aussi la commande `arp` sur l'ordinateur 192.168.1.2, et observez qu'aucun de ces deux ordinateurs ne se connaient. 

---
# Première communication, protocoles ICMP et ARP

Nous allons utiliser la commande `ping` qui permet de tester la connexion avec un ordinateur. Cette commande utilise le protocole ICMP qui permet d'envoyer des messages _Echo Request (ping)_ et des réponses _Echo Reply (pong)_ en retour.

Lorsque l'on désire comprendre ce qui se passe dans un réseau, on "écoute" ce qui arrive/part sur/depuis l'interface d'un ordinaeur... Filius permet de faire cela : via des click droits sur les ordinateurs 192.168.1.1, 192.168.1.2, 192.168.1.3, 192.168.1.4, 192.168.1.254 (cf. routeur) et 192.168.3.1, faire "afficher les échanges de données".

Quelles sont les ordinateurs qui se trouvent sur un même domaine de diffusion ?

Depuis l'ordinateur 192.168.1.1, faire en ligne de commande `ping 192.168.1.2` pour tester si l'ordinateur 2 est en vie... Si tout se passe bien, 4 paquets sont transmis et 4 sont reçus. 

Observez que l'affichage des ordinateurs en communication est (quasiment) identique pour les deux. On peut compter 10 lignes chacune décrivant l'envoi d'une trame d'une IP source vers une IP destination. On note que les 2 premières portent un protocole appelé ARP. On reviendra plus tard dessus. On note que les 8 autres portent le protocole ICMP et que les trames vont par paire (1 ping avec 1 pong).

Observez que l'affichage des autres ordinateurs/routeurs du domaine de diffusion est différent des ordinateurs en communication mais identique entre eux (et cela que ces ordinateurs soient connectés ou non sur le même switch qui joint les ordinateurs en communication).

Observez que l'affichage des ordinateurs hors domaine de diffusion est encore différent...

Avant d'aller plus loin, à l'aide de la commande `arp`, pouvez-vous dire si désormais 192.168.1.1 et 192.168.1.2 se connaissent ? 

Relancez la même commande ping une 2nd fois toujours depuis l'ordinateur 192.168.1.1. Est-ce que les affichages sur les différentes machines (en communication, hors communication mais dans le même domaine de diffusion et hors domaine de diffusion) témoignent des mêmes échanges de données ?

Observez que seul l'affichage des ordinateurs en communication a évolué mais que contrairement au premier ping, le nouvel affichage ne compte plus 10 lignes mais seulement 8 de l'ICMP. Les trames qui portaient le protocole ARP ne sont plus présentes.

Devinez-vous le rôle du protocole ARP ? Quel est-il ? Plus précisément, à quoi correspondait chacune des deux premières trames ARP ?

---
## Fonctionnement du switch

Vous avez observez le cache arp des deux ordinateurs en communication. Observez le cache des switch (en cliquant dessus). Selon vous, les switch ont-ils conscience des IP ? des adresses MAC ? 

Observez que le switch1.1 connait les adresses MAC des deux ordinateurs ayant communiqués (i.e. qui ont soit envoyé une requête soit répondu à une requête).

Observez que le switch1.2 ne connaît que l'adresse MAC de l'ordinateur qui a initié la communication, 192.168.1.1, mais pas celle de l'ordinateur qui a répondu...

Observez que si vous faîtes un ping de l'ordinateur 192.168.1.1 depuis l'ordinateur 192.168.1.2 (`ping 192.168.1.1`), outre les affichages des deux protagonistes, aucun autre ne change, non plus l'état des caches des switch.

Si maintenant depuis l'ordinateur 192.168.1.2, vous pinguez l'ordinateur 192.168.1.3. Le cache du switch1.2 évolue t'il ? 

Avez-vous des idées sur comment le switch apprend où se trouve un destinataire ? 

Par ailleurs, peut-on dire que lorsque un switch connait un destinataire (via sa MAC), il n'adresse les trames à son attention que sur le port via lequel il croit le trouver ? 

---
## Fonctionnement du routeur

Depuis l'ordinateur 192.168.1.1 faire un ping sur l'ordinateur 192.168.3.2.

L'échange de données est-il détecté depuis l'ordinateur 192.168.3.1 ?
Quel type de données entend-il ? Cela devrait confirmer ce que vous savez sur les switch désormais.

Observez le cache arp sur l'ordinateur 192.168.3.2, vous devriez y trouver l'adresse MAC de quel ordinateur ? Est-ce bien le cas ? Pourquoi ? 

Pour vous aider, passez en mode conception et observez les adresses MAC des interfaces du routeur. Que s'est-il donc passer ?

Pour aller plus loin sur les questions d'acheminements vous pouvez observez les échanges de données aux interfaces du routeur et sur 192.168.3.2. Vous y découvrirez des échanges ARP insoupçonnés. On en reparlera une prochaine fois.

