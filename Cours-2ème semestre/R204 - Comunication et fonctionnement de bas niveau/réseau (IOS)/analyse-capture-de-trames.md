# Analyse d'une capture de trames - découverte des protocoles d'interconnexion

## Objectifs

- Etre capable d'utiliser un analyseur de protocoles ("packet sniffer")
- Découvrir les caractéristiques générales et l'encapsulation des protocoles du modèles TCP/IP

Le chargé de TD pourra vous aider à démarrer en vous présentant brièvement

* l'interface de l'outil de capture que l'on utilise à savoir wireshark.
* les protocoles et services mises en oeuvre dans la capture que l'on étude dans un exercice (après avoir chargé la capture)
* l'analyse que l'on peut faire des 2 premières trames capturés et initiera la carte réseau que l'on peut en déduire des informations de liaison, de réseau et applicative disponibles

Ce TP peut se terminer dans toutes les salles machines de l'IUT et pas seulement en salle réseau !

## 1. Présentation de wireshark

Pour ce faire, on utilise le logiciel multiplateforme libre GNU Wireshark (anciennement Ethereal).

* Documentation (installer, comprendre l'interface, filtrer, analyser et capturer une trame, obtenir des statistiques, suivre une "conversation", résoudre les noms) http://www.wireshark.org/docs/wsug_html_chunked/index.html
* Exemples de captures à analyser http://wiki.wireshark.org/SampleCaptures

Outre un menu et une liste de boutons raccourcis, l'affichage se décompose en trois cadres.

- cadre 1 : Une liste de trames capturées (éventuellement en temps réel)
- cadre 2 : Le contenu décodé (couche par couche) d'une trame sélectionné dans le cadre précédent
- cadre 3 : Le contenu de la trame en hexa avec coloration de la partie correspondante à l'information sélectionnée dans le cadre précédent

## 2. Analyses de trames

### 2.1. Complément de cours

**DHCP**
* Dynamic Host Configuration Protocol
* Protocole permettant au démarrage d'une station de demander à un serveur présent sur le réseau local des paramètres IP pour accéder à l'Internet (adresse IP, masque, éventuellement adresse de la passerelle par défaut, des serveurs de noms DNS) ; permet de gérer dynamiquement un pool d'adresses IP disponibles.

**ARP**
* Address Resolution Protocol
* Protocole effectuant la traduction d'une adresse de protocole de couche réseau (typiquement une adresse IPv4) en une adresse MAC (typiquement une adresse ethernet).

**DNS**
* Domain Name System (ou système de noms de domaine)
* Service permettant de traduire un nom de domaine (intelligible par l'homme) en informations de plusieurs types qui y sont associées, notamment en adresses IP. Quelques types d'informations enregistrés (Resource Record ou RR) :
  * A record (address record) fait correspondre un nom d'hôte à une adresse IPv4 ;
  * AAAA record (IPv6 address record) fait correspondre un nom d'hôte à une adresse IPv6 ;
  * PTR (inverse de A et AAAA) fait correspondre une adresse IP à un nom d'hôte ;
  * CNAME record (canonical name record) permet de faire d'un domaine un alias vers un autre. Cet alias hérite de tous les sous-domaines de l'original ;
  * MX record (mail exchange record) définit les serveurs de courriel pour ce domaine ;
  * NS record (name server record) définit les serveurs DNS de ce domaine ;
* http://fr.wikipedia.org/wiki/Domain_Name_System//

**TCP**
* Protocole de transport de bout en bout qui assure une certaine qualité de service
* Etablissement/fermeture de connexion et acquitement des échanges de connexion et de données
* Possède un mécanisme de Contrôle d'erreurs et de retransmission. Dans une conversation, chaque machine gère un numéro de séquence qu'elle transmet à chaque message et qui correspond à la quantité d'information qu'elle a envoyée. Chaque machine utilise aussi un numéro d'acquitement qui informe à l'envoyeur la quantité d'information reçue avec succès. ACK_B = SEQ_A + LEN_A
* Possède un mécanisme de gestion de flux (windows qui spécifie la taille des données que la machine peut recevoir à un moment donné)

### 2.2 Etude globale

Charger la capture1.cap soit en double cliquant ou bien en démarrant wireshark (éventuellement en ligne de commande) et en faisant ouvrir...

#### 2.2.1 La nature des échanges

En vous aidant de

* des colonnes "protocole" et "info" du cadre 1
* du diagramme de flots Statistics > Flow Graph > All packets + General Flow (attention dans ce diagramme, l'IP et la MAC d'une même machine seront dans des colonnes différentes)

Pour chaque trame

- Quel protocole de niveau le plus haut est encapsulé ? Rappelez la fonction de celui-ci ?
- Identifiez les trames qui "travaillent" ensemble i.e. en général les couples requête et réponse.

#### 2.2.2 Cartographie du réseau

* Dessinez une cartographie du réseau (i.e. les machines/équipements en présence, les liaisons entre elles/eux leur configuration réseau et les services hébergés). Pour ce faire, consulter les couches liaison, réseau, transport et application de chaque trame pour relever les informations de la configuration réseau d'une machine/équipement (adresses MAC, IP, Masque, Routeur/Gateway, DNS) et des services qu'il héberge (serveurs DHCP, DNS, HTTP).
* Sur quelle liaison a été opérée la capture ?

### 2.3 Etude détaillée

Approfondir l'analyse de la capture précédente

#### 2.3.1. Trames 1 et 2 : DHCP

Quels sont les valeurs des champs MAC/IP src/dest de la requête ?
Quel protocole de transport est utilisé pour la requête comme pour la réponse ?
La requête transmet-elle le hostname de la machine source ?
En plus de l'adresse IP délivrée, quelles autres informations de configuration réseau sont communiquées au demandeur ? Donner ces valeurs.

#### 2.3.2. Trames 3 et 4 : ARP

Où se trouve le protocole ARP dans le modèle OSI ?
Quels sont les adresses MAC source et destination de la requête ?
A quelle machine correspond l'adresse IP demandée ? Comment se fait-il que la machine source la connait ?

#### 2.3.3. Trames 5 et 6 : DNS

Quel protocole de transport est utilisé pour la requête comme pour la réponse ?
Dans la trame requête 5 à qui appartient l'adresse IP de destination ? à qui appartient l'adresse MAC de destination ?
Quel type de requête est envoyé au serveur DNS ? Pour quel nom de domaine ?
Combien de questions et de réponses sont transmises dans la trame Query Response ?
La trame réponse redonne-t-elle la requête ?
    Qu'apprend t-on sur le nom de domaine qui avait été interrogé ?

#### 2.3.4. Trames 7 à 12

A quoi correspond cette séquence de trames ? Identifiez les 3 étapes : demande de connexion, échange de données et fin de connexion
Dans la trame 7 à qui appartient l'adresse IP de destination ? à qui appartient l'adresse MAC de destination ?
Dans la séquence de trames 7 à 9, à quoi correspond les Flags "Syn" et "Ack" ?
Faire Statistics > Flow Graph..., select TCP flow and click OK. Identifiez à quelle trame réfère les différentes flèches entre le client et le serveur. Vérifier le calcul des numéros de séquence et des numéros d'acquitements notamment en observant la longueur (champ LEN) et les numéros des trames précédentes. Tout est-il bien correct ?
La requête HTTP Get est-elle acquitée par le serveur ?
Quel est le contenu de la page renvoyée ?
Les deux protagonistes demandent-ils chacun la fin de la connexion ? Quel que soit votre réponse qui initie la fin de connexion ? Le dernier envoi du flag Fin est-il acquité ?
Cliquer sur une trame puis cliquer sur Analyse > Follow TCP/UDP Stream. De quoi rend compte cet affichage (indice ) ?

#### 2.3.5. Trames 1 à 12

Expliquez en quelques phrases le scénario qui est en train de se dérouler dans cette capture ? Autrement dit, justifier l'utilisation des protocoles dans cet ordre : DHCP, ARP, DNS, TCP, HTTP et TCP à nouveau.
Faire Statistics > Conversation et observer les échanges au niveau de chaque couche/protocole. Pourquoi au niveau Ethernet il y a seulement 2 adresses MAC distinctes si l'on ne compte pas la MAC de diffusion alors que lorsqu'on regarde les adresses IP il y en a 4 distinctes (si l'on ne compte pas l'adresse d'initialisation et de diffusion) ?
Wireshark permet de faire de la résolution de nom. C'est-à-dire que si il connait à quoi correspond les adresses MAC (e.g. Broadcast à la place de FF.FF.FF.FF.FF.FF), IP (e.g. link-local à la place de 169.254.0.0), ou numéro de ports applicatifs (e.g. http pour 80) il peut les résoudre. Ré-ouvrir la capture en autorisant tour à tour soit la résolution de nom des MAC, de Network (IP), de Transport (port). Relever dans les colonnes source, destination et info les valeurs qui changent; mais aussi au niveau des différentes couches
Le protocole Ethernet contient-il des champs dédiés au contrôle de la trame ? Si oui lesquels ? Même question avec le paquet IP ? le message TCP ? le message UDP ?

## 3. Pour aller plus loin

* Filtrage de trames
* En mode super utilisateur, wireshark permet de faire de la capture de trames
