[~]$ ip link
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN mode DEFAULT group default qlen 1000
     link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
2: enp0s31f6: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP mode DEFAULT group default qlen 1000
     link/ether b0:7b:25:1a:bc:10 brd ff:ff:ff:ff:ff:ff


[~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000 link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
     inet 127.0.0.1/8 scope host lo
        valid_lft forever preferred_lft forever
2: enp0s31f6: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc  pfifo_fast state UP group default qlen 1000
     link/ether b0:7b:25:1a:bc:10 brd ff:ff:ff:ff:ff:ff
     inet 172.21.65.102/20 brd 172.21.79.255 scope global dynamic noprefixroute enp0s31f6
        valid_lft 82002sec preferred_lft 82002sec


[~]$ ip route
default via 172.21.64.1 dev enp0s31f6 proto dhcp metric 100
default via 172.21.64.1 dev enp0s31f6 proto dhcp src 172.21.65.102 metric 1024
172.21.64.0/20 dev enp0s31f6 proto kernel scope link src 172.21.65.102 metric 100
172.21.64.1 dev enp0s31f6 proto dhcp scope link src 172.21.65.102 metric 1024


[~]$ ss -tup
Netid    State    Recv-Q    Send-Q                Local Address:Port          Peer Address:Port      Process
udp      ESTAB    0         0            172.21.65.90%enp0s31f6:bootpc         172.21.0.77:bootps
tcp      ESTAB    0         0                      172.21.65.90:33498         172.21.0.201:ldaps
tcp      ESTAB    0         0                      172.21.65.90:38159         172.21.0.206:1010
tcp      ESTAB    0         0                      172.21.65.90:936           172.21.0.206:nfs


[~]$ ss -tupn
Netid        State        Recv-Q        Send-Q                       
Local Address:Port                Peer Address:Port        Process
udp          ESTAB        0             0                   172.21.65.90%enp0s31f6:68                   172.21.0.77:67
tcp          ESTAB        0             0                             172.21.65.90:33498               172.21.0.201:636
tcp          ESTAB        0             0                             172.21.65.90:38159               172.21.0.206:1010
tcp          ESTAB        0             0                             172.21.65.90:936                 172.21.0.206:2049



[~]$ ss -tup # après lancement de firefox
Netid    State    Recv-Q    Send-Q                 Local Address:Port         Peer Address:Port     Process
udp      ESTAB    0         0            172.21.65.102%enp0s31f6:bootpc        172.21.0.78:bootps
tcp      ESTAB    0         0                      172.21.65.102:47354          172.21.0.52:3128      users:(("firefox-bin",pid=3656,fd=144))
tcp      ESTAB    0         0                      172.21.65.102:47142          172.21.0.52:3128      users:(("firefox-bin",pid=3656,fd=121))
tcp      ESTAB    0         0                      172.21.65.102:47122          172.21.0.52:3128      users:(("firefox-bin",pid=3656,fd=69))
tcp      ESTAB    0         0                      172.21.65.102:47214          172.21.0.52:3128      users:(("firefox-bin",pid=3656,fd=123))
tcp      ESTAB    0         0                      172.21.65.102:46604          172.21.0.52:3128      users:(("firefox-bin",pid=3656,fd=129))
tcp      ESTAB    0         0                      172.21.65.102:47138          172.21.0.52:3128      users:(("firefox-bin",pid=3656,fd=119))
tcp      ESTAB    0         0                      172.21.65.102:47128          172.21.0.52:3128      users:(("firefox-bin",pid=3656,fd=83))
tcp      ESTAB    0         0                      172.21.65.102:47118          172.21.0.52:3128      users:(("firefox-bin",pid=3656,fd=63))
tcp      ESTAB    0         0                      172.21.65.102:47116          172.21.0.52:3128      users:(("firefox-bin",pid=3656,fd=10))
tcp      ESTAB    0         0                      172.21.65.102:47136          172.21.0.52:3128      users:(("firefox-bin",pid=3656,fd=114))
tcp      ESTAB    0         0                      172.21.65.102:47130          172.21.0.52:3128      users:(("firefox-bin",pid=3656,fd=93))
tcp      ESTAB    0         0                      172.21.65.102:47132          172.21.0.52:3128      users:(("firefox-bin",pid=3656,fd=94))
tcp      ESTAB    0         0                      172.21.65.102:766           172.21.0.206:nfs
tcp      ESTAB    0         0                      172.21.65.102:47126          172.21.0.52:3128      users:(("firefox-bin",pid=3656,fd=81))
tcp      ESTAB    0         0                      172.21.65.102:47134          172.21.0.52:3128      users:(("firefox-bin",pid=3656,fd=97))
tcp      ESTAB    0         0                      172.21.65.102:40304         172.21.0.201:ldaps
tcp      ESTAB    0         0                      172.21.65.102:47124          172.21.0.52:3128      users:(("firefox-bin",pid=3656,fd=79))
tcp      ESTAB    0         0                      172.21.65.102:47120          172.21.0.52:3128      users:(("firefox-bin",pid=3656,fd=68))