# R2.04 Communication et fonctionnement bas niveau

Matériel pédagogique pour la ressource Communication et fonctionnement bas niveau du BUT informatique à l'IUT de Nantes.

## Contenu du dépôt

- td-pile-tas : un TD présentant l'organisation générale de la mémoire d'un processus avec des exemples en Go.
