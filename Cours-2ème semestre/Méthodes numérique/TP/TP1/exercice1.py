import matplotlib.pyplot as plt
import numpy as np
from matplotlib.patches import Patch


# Exercice 1.1

def f(c, x):
    return c*x*(1-x)

def figure1() :
    absc = np.linspace(0, 1, 100)

    ordo1 = f(0.5,absc)
    ordo2 = f(1.5,absc)
    ordo3 = f(2,absc)
    ordo4 = f(2.5,absc)
    ordo5 = f(3,absc)
    ordo6 = f(3.5 ,absc)
    ordo7 = f(4,absc)

    fig, ax = plt.subplots()

    ax.plot(absc, ordo1)
    ax.plot(absc, ordo2)
    ax.plot(absc, ordo3)
    ax.plot(absc, ordo4)
    ax.plot(absc, ordo5)
    ax.plot(absc, ordo6)
    ax.plot(absc, ordo7)
    ax.plot([0,1], [0,1])   # axe x = y

    legend_def = ['x = y', 'c = 0.5', 'c = 1.5', 'c = 2', 'c = 2.5', 'c = 3', 'c = 3.5', 'c = 4']
### afficher la legend ( avec l'utilisation des lsite plus haust)
    liste_legend = [Patch(edgecolor = 'black', label= legend_def[i]) for i in range (8)]

    plt.legend(handles = liste_legend, loc='upper left')
    plt.show()

figure1()

def logistique(c, u0, N):
    L = [u0]
    for i in range(N):
        L.append(f(c, L[i]-1))

    absc = np.linspace(0, N, N+1)
    ordo = L

    fig, ax = plt.subplots()

    ax.plot(absc, ordo)
    plt.show()

    return L

logistique(2.3, 0.2, 10)

def figure2():

    c = 2.3
    u0 = 0.2

    absc = np.linspace(0, 1, 200)
    ordo7 = f(c, absc)
    print(logistique(c, u0, 10))

    fig, ax = plt.subplots()

    ax.plot(absc, ordo7)
    ax.plot([0,1], [0,1]) 
    message = "c = ", c, "et u0 = ", u0
    plt.title(message)
    plt.show()

figure2()

