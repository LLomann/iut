import numpy as np

#exercice 2.1______________Méthode par Dichotomie______________

#1) Cette méthode converge bien vers Alpha car elle 
# permet de réduir l'intervalle [a;b] pour ce raprocher 
# de plus en plus du milieu alpha


#2)
def dichotomie(f, a, b, e):
    if e <= 0:
        return "e doit être supérieur à 0"

    m = (a+b)/2

    if abs(b-a)<e:
        return "alpha vaut :", m, "et f(alpha) vaut :", f(m)

    if f(m)*f(a) < 0:
        return dichotomie(f, a, m, e)
    else:
        return dichotomie(f, m, b, e)


#3)
def g(x):
    return x*np.tan(x) - 1

def h(x):
    return x**2 - 2

print(dichotomie(g, -100, 100, 10**-6))
print(dichotomie(h, -6, 6, 10**-6))



#exercice 2.3______________Méthode de Newton______________


#1) tangente a la courbe représentatif de f(x) au point d'abcisse x0: y = f'(x0) * (x-x0) + f(x0)
# on cherche le point qi coupe l'axe des abcisse, donc y = 0, on vas donc chercher a résoudre :
# f'(x0) * (x-x0) + f(x0) = 0 la resolution de cette équation nous donnera x1

# f'(x0) * (x-x0)   = - f(x0)
# x-x0              = - (f(x0) / f'(x0))
# x                 = x0 - (f(x0) / f'(x0))

# on a donc x1 = x0 - (f(0) / f'(x) )


#2) x0 appartiens a R
#   xn+1 = 𝜙(xn)

# avec

# 𝜙:  R -> R
#     x -> x- (f(x) / f'(x))


#3
def Newton(f, df, x0, N):

    x1 = x0 - (f(x0) / df(x0))
    N = N-1

    if N > 0:
        return Newton(f, df, x1, N)
    else:
        return "la valeur approcher de a est XN est :", x1

def f(x):
    return x**2 - 2

def df(x):
    return 2*x


#4
print(Newton(f, df, 3, 10))
print(Newton(f, df, 3, 15))

print(Newton(f, df, 5, 10))
print(Newton(f, df, 22, 10))

#5