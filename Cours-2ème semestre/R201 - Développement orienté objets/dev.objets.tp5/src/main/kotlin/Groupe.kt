class Groupe(objet1: ObjetGraphique, objet2: ObjetGraphique): ObjetGraphique() {

    private val objetsRegroupes: Array<ObjetGraphique?>

    init {
        objetsRegroupes = arrayOfNulls(10)
        objetsRegroupes[0] = objet1
        objetsRegroupes[1] = objet2
    }

    override fun selectionner(ok: Boolean){
        for (monObjet in objetsRegroupes) {
            if (monObjet!= null) {
                monObjet.selectionner(ok)
            }
        }
    }

    fun regrouper(objetAjoute: ObjetGraphique){
        if (null in objetsRegroupes && objetAjoute !in objetsRegroupes){
            objetsRegroupes[objetsRegroupes.indexOf(null)] = objetAjoute
        }
    }


}