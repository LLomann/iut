class Feuille(): Redimensionnable {

    private var hauteur: Int = 200
    private var largeur: Int = 100
    private val objetsContenus: Array<ObjetGraphique?>

    init{
        objetsContenus = arrayOfNulls(100)
    }

    override fun redimensionner(nouvelleHauteur: Int, nouvelleLargeur: Int) {
        hauteur = nouvelleHauteur
        largeur = nouvelleLargeur
    }

    fun inserer( nouvelObjet: ObjetGraphique){
        if (null in objetsContenus && nouvelObjet !in objetsContenus) {
            objetsContenus[objetsContenus.indexOf(null)] = nouvelObjet
        }
    }

}