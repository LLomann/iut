class Camion(places: Int) {

    private var placesOccupees: Int = 0
    private val remorque: Array<Voiture?>

    init{
        remorque = arrayOfNulls<Voiture>(places)
    }

    fun estPleins() = (placesOccupees == remorque.size)

    fun estVide() = (placesOccupees == 0)

    fun charger(voitureTransportee: Voiture): Boolean{

        if (estPleins()){
            return false
        }
        if (voitureTransportee.estEnMarche()){
            return false
        }
        if (voitureTransportee in remorque){
            return false
        }
        remorque[placesOccupees] = voitureTransportee
        placesOccupees++
        return true
    }

    fun decharger(): Voiture?{

        if (estVide(){
            return null
        }
        placesOccupees --
        val aDecharger = remorque[placesOccupees]
        remorque[placesOccupees] = null
        return aDecharger
    }

}