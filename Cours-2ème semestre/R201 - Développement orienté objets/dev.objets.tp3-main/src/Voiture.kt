class Voiture (mod: String, coul: String, vitMax: Double){
    private var modele: String
    private var couleur: String
    private var vitesseCourante: Double = 0.0
    private var vitesseMaximum: Double
    private var enMarche: Boolean = false
    private var proprietaire: Personne?
    private var parking: Parking?

    init{
        modele = mod
        couleur = coul
        vitesseMaximum = vitMax
        proprietaire = null
        parking = null
    }

    fun acheter(acheteur: Personne){
        acheteur = proprietaire
    }

    fun demarrer(){
        enMarche = true
    }

    fun arreter(){
        vitesseMaximum = 0
        enMarche = false
    }

    fun estEnMarche(): Boolean{

        if (demarrer()){
            return true
        }
        return false
    }

    fun repeindre(nouvelleCouleur: String){
        couleur = nouvelleCouleur
    }

    fun accelerer(acceleration: Double): Double{
        if (vitesseCourante + acceleration <= vitesseMaximum && acceleration>=0) {
            return (vitesseCourante += acceleration)
        }
    }

    fun decelerer(deceleration: Double): Double{
        if (vitesseCourante + deceleration >=0 && deceleration<=0) {
            return (vitesseCourante += deceleration)
        }
    }

    fun estGaree(){
        enMarche = false
    }

    fun stationner(nouveauParking: Parking){
        arreter()
        parking = nouveauParking
    }

    fun quitterStationnement(){
        parking = null
        demarrer()
    }

    fun afficher(){
        println("Voiture $modele, de couleur $couleur, ayant une vitesse courante de $vitesseCourante et une vitesseMaximum de $vitesseMaximum.")
        if (proprietaire !== null){
            println("le proprietaire de la voiture est $proprietaire")
        }
    }
}