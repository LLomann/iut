class Parking (places: Int) {

    private var voiture: Voiture?
    private var places: Int

    init{
        this.places = places
        parking = null
    }

    fun nombreDePlace(): Int{
        var compteur = 0
        for (place in stationnement) {
            if (place == null) compteur ++
        }
        return compteur
    }

    fun nombreDePlaceTotales() stationnement.size

    fun placeLibre(numeroPlace: Int): Boolean{
        if (numeroPlace < 0 || numeroPlace >= stationnement.size){
            return false
        }else{
            return stationnement[numeroPlace] == null
        } 
    }

    fun stationner(numeroPlace: Int, voitureStationnee: Voiture): Boolean{

        if (!placeLibre(numeroPlace)){
            return false
        }
        if (voitureStationnee in stationnement){
            return false
        }
        stationnement.set(numeroPlace, voitureStationne)
        return true
    }
}