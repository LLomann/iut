class Espece {
   - nom: String
}

class Personnel {
   - nom: String
   - prenom: String
   - adress: String
}

class Affectation {
   - nom: String
}

class Vaisseau {
   - immatriculation: String
}

class Base {
}

class Planet {
   - nom: String
   - prenom: String
}

Affectation <|-- Base
Affectation <|-- Vaisseau


Personnel "1" --> "0...*" Espece : à pour espece
Personnel "0...*" --> "1" Planet : est née sur
Personnel "0..*" -- "1" Affectation : est affecter

Vaisseau "1" --> "0...*" Personnel : à un capitaine

Espece "0...*" --> "1...*" Planet : est originaire de

Base "1" --> "1" Planet : est en orbite


