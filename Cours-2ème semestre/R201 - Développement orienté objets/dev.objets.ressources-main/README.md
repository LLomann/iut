# A propos de dev.objets.ressources

Page d'information à propos du cours **"Développement Orienté Objets – Ressource R2.01"**.

Vous y trouverez les .PDF des cours, des exemples, les sujets de TPs, etc.

## Supports de cours

[CM0 > introduction au module](./00-intro.pdf)

[CM1 > introduction à Kotlin : les bases du langage](01-bases-kotlin.pdf)

[CM2 > introduction à la programmation objet + manipulation d'objets avec Kotlin](02-des-objets.pdf)

[CM3 > classes : modélisation UML et implémentation Kotlin](03-classes+uml.pdf)

## TDs + TPs

[TP1 : introduction à Kotlin](https://gitlab.univ-nantes.fr/iut.info1.dev.objets/dev.objets.tp1)

[TP2 : manipuler des objets en Kotlin](https://gitlab.univ-nantes.fr/iut.info1.dev.objets/dev.objets.tp2)

[TD2 : ligne de commande et reférences mémoire](TDs/td2_etu.pdf) + [sources Kotlin](https://gitlab.univ-nantes.fr/iut.info1.dev.objets/dev.objets.td2)

[TP3 : implémentation de classes Koltin d'après un diagramme UML](https://gitlab.univ-nantes.fr/iut.info1.dev.objets/dev.objets.tp3)


## Installer Kotlin chez vous

Pour travailler sur votre ordinateur personnel, il est nécessaire que vous installiez le compilateur kotlin en ligne de commande (ainsi que l'équivalent d'un terminal pour les non-informaticiens travaillant sous Windows) : [Téléchargement de Kotlin 1.6.10](https://github.com/JetBrains/kotlin/releases/tag/v1.6.10) et [Kotlin command-lin compiler documentation](https://kotlinlang.org/docs/command-line.html) 



Il va aussi vous être nécessaire d'installer Ant : [procédure d'installation d'Ant](https://ant.apache.org/manual/install.html)
