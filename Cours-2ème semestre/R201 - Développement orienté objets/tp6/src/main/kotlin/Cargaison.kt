abstract class Cargaison(distance: Int) {

    private var distance : Int
    private var encombrementActuel: Double = 0.0
    private var listeMarchandise : Array<Marchandise?>

    init{
        this.distance = distance
        listeMarchandise = arrayOfNulls(100)
    }

    fun ajouter(marchandiseAjoutee: Marchandise): Boolean{
        if( encombrementActuel + encombrement(marchandiseAjoutee) <= limite()){
            if (null in listeMarchandise && marchandiseAjoutee !in listeMarchandise ){
                listeMarchandise[listeMarchandise.indexOf(null)] = marchandiseAjoutee
                return true
            }
        }
        return false
    }

    fun cout(): Double = encombrementActuel*distance*facteur()

    fun recherche(marchandiseRecherchee: Marchandise): Int {
        if(marchandiseRecherchee in listeMarchandise)
            return listeMarchandise.indexOf(marchandiseRecherchee)

    }

    protected open fun encombrement(marchandise: Marchandise): Double = marchandise.donnePoids()

    protected abstract fun limite(): Double

    protected open fun facteur(): Int = 1



}