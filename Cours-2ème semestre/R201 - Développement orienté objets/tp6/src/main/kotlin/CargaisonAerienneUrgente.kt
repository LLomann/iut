class CargaisonAerienneUrgente(distance: Int): CargaisonAerienne(distance) {

    protected override fun facteur(): Int = 2*super.facteur()
}