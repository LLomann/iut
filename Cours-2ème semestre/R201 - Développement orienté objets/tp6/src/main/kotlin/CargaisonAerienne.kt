class CargaisonAerienne(distance: Int): Cargaison(distance) {

    protected override fun encombrement(marchandise: Marchandise): Double = marchandise.donneVolume()

    protected override fun limite(): Double = 80000.0

    protected override fun facteur(): Int = 10
}