class Document(nomDeFichier: String): Editable {

    private var nomDeFichier: String
    private var feuillesAssociees: Array<Feuille?>

    init{
        this.nomDeFichier = nomDeFichier
        feuillesAssociees = arrayOfNulls(10)
        nouvelleFeuille()
    }

    override fun editer(nouveau: String) {
        nomDeFichier = nouveau
    }

    fun nouvelleFeuille(): Feuille?{
        if(null in feuillesAssociees){
            var nmFeuille: Feuille = Feuille()
            feuillesAssociees[feuillesAssociees.indexOf(null)] = nmFeuille
            return nmFeuille
        }
        return null

    }

    fun donneFeuille(position: Int): Feuille? {
        if (position in feuillesAssociees.indices) {
            return feuillesAssociees[position]
        }
        return null
    }

}