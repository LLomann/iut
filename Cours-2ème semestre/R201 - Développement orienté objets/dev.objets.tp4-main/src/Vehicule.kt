class Vehicule (mod: String, coul: String, vitMax: Double):Propriete {
    
    private var modele:             String
    private var couleur:            String
    private var vitesseCourante:    Double  = 0.0
    private var vitesseMaximum:     Double
    private var enMarche:           Boolean = false
    private var proprietaire:       Proprietaire?
    private var conducteur:         Personne?

    init{
        modele =            mod
        couleur =           coul
        vitesseMaximum =    vitMax
        proprietaire =      null
        conducteur =        null
    }

    override fun acheter(acheteur : Proprietaire){
        proprietaire = acheteur
    }

    fun devientConducteur(personne: Personne){
        conducteur = personne
    }

    fun plusDeConducteur() {
        conducteur = null
        arreter()
    }

    fun demarer() {
        if (!(enMarche)) {
            enMarche = true
        }
    }

    fun arreter() {
        if (enMarche) {
            enMarche = false
            vitesseCourante = 0.0
        }
    }

    fun estEnMarche(): Boolean {
        return enMarche
    }

    fun repeindre(nouvelleCouleur: String) {
        couleur = nouvelleCouleur
    }

    fun accelerer(acceleration: Double): Double {

        if (acceleration > 0.0) {
            if (enMarche) {
                if (vitesseCourante + acceleration < vitesseMaximum ) {
                    vitesseCourante += acceleration
                }
                else{
                    vitesseCourante = vitesseMaximum
                }
            }
        }
        return vitesseCourante
    } 

    fun decelerer(deceleration: Double): Double {

        if (deceleration > 0.0) {
            if (enMarche) {
                if (vitesseCourante - deceleration > 0) {
                    vitesseCourante -= deceleration 
                }
                else{
                    vitesseCourante = 0.0
                    enMarche = false
                }
            }
        }
        return vitesseCourante
    }

}