class PizzaEmbalable(nom: String, forme: Forme): Pizza(nom,forme), Embalable() {
    private var estEmballee : Boolean

    init{
        estEmballee = false
    }

    override fun emballer(ok: Boolean) {
        estEmballee = ok
    }

    override fun donneCout(): Double {
        var coutsup : Double = 0.0
        if (estEmballee) {
            coutsup = 2.0
        }
        return super.donneCout() + coutsup
    }
}