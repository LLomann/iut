import kotlin.math.PI

class Cercle(rayon : Double): Forme() {
    private var rayon : Double
    init{
        this.rayon = rayon
    }


    override fun donneSurface(): Double = PI* rayon * rayon
}