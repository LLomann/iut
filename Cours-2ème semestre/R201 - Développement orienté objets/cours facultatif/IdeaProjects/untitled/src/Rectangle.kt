class Rectangle(largeur: Double, hauteur: Double): Forme(){
    private var largeur : Double
    private var hauteur : Double

    init{
        this.largeur = largeur
        this.hauteur = hauteur
        nom = "Rectangle"
    }

    override fun donneSurface(): Double = (largeur * hauteur)

}