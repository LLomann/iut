package but1.iut.r203

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource

class OperationsBinairesTest {


    @ParameterizedTest
    @CsvSource(
        "3,3,6",
        "1,2,3",
        "2,1,3",
        "3,2,6",
        "-2,0,-2"
    )
    fun diviserNaturel_(a: Int, b: Int, oracle: Int) {
        assertEquals(oracle, OperationsBinaires().additionner(a, b))
    }






}