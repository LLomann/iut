import json
import string
import unicodedata
import matplotlib.pyplot as plt
import numpy as np

#__________________Exercice3.2__________________#
#1)
alphaMinus = string.ascii_lowercase
alphaMaj = string.ascii_uppercase

def caesar_crypt(msg, decal):

    msg = unicodedata.normalize('NFD', msg)
    msg = msg.encode('ascii', 'ignore')
    msg = str(msg.decode('utf-8'))

    newMsg = ''
    for i in msg:
        if i.isalpha():
            if i in alphaMinus:
                indexLettre = alphaMinus.index(i) + decal
                newMsg += alphaMinus[indexLettre%26]
            else :
                indexLettre = alphaMaj.index(i) + decal
                newMsg += alphaMaj[indexLettre%26]
        else :
            newMsg += i
    return newMsg


msg_caesar = open("encrypted_with_caesar.txt")
true_msg = ""
for i in msg_caesar:
    true_msg += i
print(true_msg)

print(caesar_crypt(true_msg,9))


#__________________Exercice3.2__________________#
#2.a)

def bar_ferq(fichier):
    
    fichier = open(fichier, "r")
    dico = json.load(fichier)

    liste = dico.items()
    #print(liste)

    # x,y = zip(*liste)

    x = [i[0] for i in liste] 
    y = [i[1] for i in liste]

    plt.bar(x, y)
    plt.show()


bar_ferq("freq_lettre_fr.json")

#b)

def bar_ferq1_freq2(fichier1, fichier2):
    
    fichier1 = open(fichier1, "r")
    dico1 = json.load(fichier1)
    fichier2 = open(fichier2, "r")
    dico2 = json.load(fichier2)

    #print("dico1 :", dico1)
    #print("dico2 :", dico2)

    for cle, valeur in dico1.items():
        dico2[cle] += valeur
    #print ("dico2 + dico1 =", dico2)

    xx = [i[0] for i in dico1.items()]
    yy = [i[1] for i in dico2.items()]

    plt.bar(xx,yy)
    plt.show()

bar_ferq1_freq2("freq_lettre_fr.json","freq_lettre_fr.json")

#c)


   
#d

#3.a)
# def correlation_freqs(fichier):
