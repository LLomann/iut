import unicodedata
import random
import string
import os
import json
import matplotlib.pyplot as plt
import numpy as np

from matplotlib.cbook import ls_mapper

#__________________Exercice3.3__________________#

#1)

def crypt(msg) :

    #_____enleve ponctation, accent ... au_____#

    msg = msg.upper()
    msg = unicodedata.normalize('NFD', msg)
    msg = msg.encode('ascii', 'ignore')
    msg = str(msg.decode('utf-8'))

    print(msg)

    nouveau_msg = ""

    for i in msg:
        if i.isalpha():
            nouveau_msg += i

    print(nouveau_msg)

    #_____numéro aléatoire_____#

    decal = random.randint(0,25)
    print(decal)

    #_____permutation des lettres_____#

    alphabet = string.ascii_uppercase

    msg_permuter = ''
    for i in nouveau_msg:

        indexLettre = alphabet.index(i) + decal
        msg_permuter += alphabet[indexLettre%26]

    print(msg_permuter)

crypt("Test plus complexe : àèé  @, 9 $        ***")

#2)
# Le nombre de chiffrement possible avec cette méthode est de 26. 
# Puisuq'il n'y a pas beaucoup de chiffrement différents possible, 
# il est possiblede vouloir déchiffrer ce message en testant tout 
#les chiffrement possible

#3)
def chiffre(msg, key):

    msg = msg.upper()
    msg = unicodedata.normalize('NFD', msg)
    msg = msg.encode('ascii', 'ignore')
    msg = str(msg.decode('utf-8'))

    print(msg)

    nouveau_msg = ""

    for i in msg:
        if i.isalpha():
            nouveau_msg += i

    print(nouveau_msg)

    #_____permutation des lettres_____#

    alphabet = string.ascii_uppercase

    msg_permuter = ''
    for i in nouveau_msg:
        indexLettre = alphabet.index(i) + key
        msg_permuter += alphabet[indexLettre%26]

    print(msg_permuter)

chiffre("test pour voir si tout est décaler de 4 et si toute la ponctuation sans vas", 4)

#4.a)


print("texte_chiffre_substitution.txt")

text = ''

new_file = open("texte_chiffre_substitution.txt","r")
textTempo = new_file.read().lower()
new_file.close()

textTempo = unicodedata.normalize('NFD', textTempo)
textTempo = textTempo.encode('ascii', 'ignore')
textTempo = str(textTempo.decode('utf-8'))

text += textTempo

dico = {}

for i in text :
    if i.isalpha():         
        if i not in dico :
            dico[i] = 1
        else : 
            dico[i] += 1

print(dico)

new_file = open( "freq_lettre_fr_approx.json","w")
json.dump(dico,new_file, indent= 4 )
new_file.close()

#4.b)

#5)

def logscore(fichier):

    fichier = open(fichier, "r")
    dico = json.load(fichier)

    print(dico)

print(logscore("quadrigramme.txt"))