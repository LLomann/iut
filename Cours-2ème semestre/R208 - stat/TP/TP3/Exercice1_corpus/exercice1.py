import json
import unicodedata
import os

#__________________Exercice3.1__________________#


print(os.listdir("Corpus"))

text = ''

for nomFichier in os.listdir("Corpus"):

    new_file = open("Corpus/" + nomFichier,"r")
    textTempo = new_file.read().lower()
    new_file.close()

    textTempo = unicodedata.normalize('NFD', textTempo)
    textTempo = textTempo.encode('ascii', 'ignore')
    textTempo = str(textTempo.decode('utf-8'))

    text += textTempo


dico = {}

for i in text :
    if i.isalpha():         
        if i not in dico :
            dico[i] = 1
        else : 
            dico[i] += 1

print(dico)

new_file = open( "freq_lettre_fr_approx.json","w")
json.dump(dico,new_file, indent= 4 )
new_file.close()