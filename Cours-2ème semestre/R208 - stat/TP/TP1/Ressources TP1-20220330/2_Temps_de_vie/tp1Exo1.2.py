import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

##EXERCICE 1.2 ___________________________________________________________________________________##

data = pd.read_csv('temps_de_vie.csv', sep=',')
data.rename(columns={data.columns[0]:'a',data.columns[1]:'b'}, inplace=True)

plt.hist(data['b'])

plt.show()

##___________________________________________________________________________________##