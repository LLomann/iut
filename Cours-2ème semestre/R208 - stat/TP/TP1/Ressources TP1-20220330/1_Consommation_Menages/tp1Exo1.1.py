import pandas as pd
import matplotlib.pyplot as plt
import numpy as np


##EXERCICE 1.1 ___________________________________________________________________________________##

data = pd.read_csv('consommation_menages.csv', sep=',', decimal=',')
import matplotlib.pyplot as plt
import numpy as np

type(data)
print(data.info)
print(data.Fonction)

print(data['Fonction'])
print(data['1960'])
print(data[['1960','2020']])

##___________________________________________________________________________________##


# # Barres 1960
fig, ax = plt.subplots()

pt = []

for i in data['1960']:
    pt.append(i)
pt.sort()

ax.bar(data['Fonction'],pt)
plt.xticks(rotation=45)
plt.show()

##___________________________________________________________________________________##


# Camembert 2020

plt.pie(data['2020'], labels=data['Fonction'])
plt.show()

##___________________________________________________________________________________##


# Barres 1960 2020

labels = data['Fonction']
men_means = data['1960']
women_means = data['2020']

x = np.arange(len(labels)) 
width = 0.35  # taille des barres

fig, ax = plt.subplots()
rects1 = ax.bar(x - width/2, men_means, width, label='1960')
rects2 = ax.bar(x + width/2, women_means, width, label='2020')
ax.set_ylabel('Scores')
ax.set_title('Scores by group and gender')
ax.legend()

fig.tight_layout()

plt.show()

