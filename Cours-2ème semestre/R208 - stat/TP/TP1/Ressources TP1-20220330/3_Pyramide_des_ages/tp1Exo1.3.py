import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

##EXERCICE 1.1 ___________________________________________________________________________________##


data = pd.read_csv('pyramide_ages.csv', sep=',')

print(data.columns)
print(data['age'])
print(data[data['age']<60])
## renvois un tableau de pyramide_age.csv dont l'age est est infèreieur à 60 ans

## ___________________________________________________________________________________##


data = pd.read_csv('../3_Pyramide_des_ages/pyramide_ages.csv', sep=',')


# 2019

fig, axes = plt.subplots(1,1)

axes.barh(data["age"], width = data['2019_F'], height= 0.80, label="Femmes")
axes.barh(data["age"], width = -data['2019_H'], height=0.80, label="Hommes")
axes.set_title("Pyramide des âges en France en 2019")
axes.set(xlabel='Pop', ylabel='Âge')
plt.legend(loc='upper right')
plt.show()


## ___________________________________________________________________________________##


# 1975

ig, axes = plt.subplots(1,3)

# 1975
axes[0].barh(data["age"], width = data['1975_F'], height= 0.80, label="Femmes")
axes[0].barh(data["age"], width = -data['1975_H'], height=0.80, label="Hommes")
diff = data['1975_F'] - data['1975_H']
axes[0].set_title("Pyramide des âges en France en 1975")
axes[0].plot(diff,data["age"], color="green", label="Différence")
axes[0].set(xlabel='Pop', ylabel='Âge')

# 1999
axes[1].barh(data["age"], width = data['1999_F'], height= 0.80, label="Femmes")
axes[1].barh(data["age"], width = -data['1999_H'], height=0.80, label="Hommes")
diff = data['1999_F'] - data['1999_H']
axes[1].set_title("Pyramide des âges en France en 1999")
axes[1].plot(diff,data["age"], color="green", label="Différence")
axes[1].set(xlabel='Pop', ylabel='Âge')


# 2019
axes[2].barh(data["age"], width = data['2019_F'], height= 0.80, label="Femmes")
axes[2].barh(data["age"], width = -data['2019_H'], height=0.80, label="Hommes")
diff = data['2019_F'] - data['2019_H']
axes[2].set_title("Pyramide des âges en France en 2019")
axes[2].plot(diff,data["age"], color="green", label="Différence")
axes[2].set(xlabel='Pop', ylabel='Âge')

plt.legend(loc='upper right')
plt.show()