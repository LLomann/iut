from cgitb import handler
import geopandas as gpd
import pandas as pd
import matplotlib.pyplot as plt
import contextily as ctx
from matplotlib.patches import Patch


### Lecture du fichier
data = gpd.read_file("../a-dep2021.json")
print(data)

### Ouverture d’une figure
fig, axes = plt.subplots(1,1,figsize=(12,12))

###on rattache nos donnØes à cette carte avec l’ajout d’une ligne
data = data.to_crs(epsg=3857)

### Code d ́epartementaux des DOM
filter_dep = ["971", "972", "973", "974", "976"]

### On filtre nos donn ́ees
data = data.query('dep not in @filter_dep')

### import les données du csv dans le dossier json
dataCol = gpd.read_file("collegiens_REP.csv")
dataPart = dataCol['Part_collegien_REP']
print(dataPart)

data['Part_collegiens'] = pd.DataFrame(dataPart)
print(data)
data['Color'] = '#FFFFFF'

# change la couleur en fonction du nombre de Part_collegiens
for i, colone in data.iterrows():

    if float(colone['Part_collegiens']) < 1:
        data.at[i, 'Color'] = '#FFFFFF'

    elif float(colone['Part_collegiens']) < 2.5:
        data.at[i, 'Color'] = '#E2B5A4'

    elif float(colone['Part_collegiens']) < 4.5:
        data.at[i, 'Color'] = '#C48B7D'

    elif float(colone['Part_collegiens']) < 7.5:
        data.at[i, 'Color'] = '#A76258'

    elif float(colone['Part_collegiens']) < 12.5:
        data.at[i, 'Color'] = '#8A3832'
        
    else:
        data.at[i, 'Color'] = '#6C0000'

print(data)

### Trac ́e des donn ́ees g ́eographiques dans la figure
data.plot(
ax = axes,          # Axes de trac ́e
linewidth=0.8,      # Epaisseur de la ligne
edgecolor='black',  # Couleur de la ligne
color= data.Color,  # Couleur de l’aplat
alpha = 0.5         # Transparence de l’aplat

)
couleur = ['#FFFFFF', '#E2B5A4', '#C48B7D', '#A76258', '#8A3832', '#6C0000']
legend_def = ['Aucune REP+', 'Moins de 2,5', 'De 2,5 à moins de 4,5', 'De 4,5 à moins de 7,5', 'De 7,5 à moins de 12,5', '12,5 ou plus']

### afficher la legend ( avec l'utilisation des lsite plus haust)
liste_legend = [Patch(facecolor = couleur[i], edgecolor = 'black', label= legend_def[i]) for i in range ( len(couleur))]

plt.legend(handles = liste_legend, loc='upper right', title="Part de collégiens en REP+ (en %)")


ctx.add_basemap(ax=axes)
axes.set_axis_off() # Suppression des axes
### Affichage
plt.show()







