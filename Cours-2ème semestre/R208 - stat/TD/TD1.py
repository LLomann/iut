from math import *


def mean(L):
    compte = 0
    for i in range (len(L)):
        compte += L[i]
    return compte/len(L)

liste = [1,5,12,6,20,20]

print(mean(liste))


def variance(L):
    L2 = []
    for i in L:
        L2.append(i**2)
    return mean(L2) - mean(L)**2

print(variance(liste))

def ecart_type(L):
    return sqrt(variance(L))

print(ecart_type(liste))