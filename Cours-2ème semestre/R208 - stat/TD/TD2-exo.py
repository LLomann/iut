from math import *
import matplotlib.pyplot as plt
import numpy as np

x = [121,123,108,118,111,109,114,103,110,115]
y = [52,22,19,24,19,18,20,15,20,21]

def moyenne(liste):
    somme = 0
    for i in range (len(liste)):
        somme += liste[i]
    return somme/(len(liste))

def moyenne2(liste):
    somme = 0
    for el in liste:
        somme += el
    return somme/(len(liste))

def moyenneXY(liste1,liste2): 
    num = 0
    for i in range (len(liste1)):
        num += liste1[i] * liste2[i]
    return (num/len(liste1))

def coovariance(liste1, liste2):
    return moyenneXY(liste1, liste2) - moyenne(liste1) * moyenne(liste2)

def variance(liste):
    sommeCarre = 0
    for i in range (len(liste)):
        sommeCarre += liste[i]**2
    return (sommeCarre/len(liste)) - moyenne(liste)**2

def droite_regretion_lineaire(liste1, liste2):
    return  print(' équation droite de régrétion linéaire, y =', (coovariance(liste1,liste2)/variance(liste1)), 'x +', moyenne(liste2)-(coovariance(liste1,liste2)/variance(liste1)*moyenne(liste1)))

def droite_regretion_lineaire_graphe(liste1, liste2, x):
    return (coovariance(liste1,liste2)/variance(liste1) * x) + moyenne(liste2)-(coovariance(liste1,liste2)/variance(liste1)*moyenne(liste1))



def ecart_type(liste):
    return sqrt(variance(liste))

def coeff_correlation_lineaire(liste1, liste2):
    return coovariance(liste1,liste2)/ (ecart_type(liste1) * ecart_type(liste2))



listeX = [20,30,50,70,90,110,130]
listeY = [13,28,42,66,96,129,167]


for i in range (len(listeY)):
    listeY[i] = listeY[i]**2
print(listeY)

print(' moyenne de x :', moyenne(listeX))
print(' moyenne de y :', moyenne(listeY))
print(' moyenne de xy :', moyenneXY(listeY, listeX))
print(' coovariance xy :', coovariance(listeY, listeX))
print(' variance x :', variance(listeX))
droite_regretion_lineaire(listeX, listeY)
print(' variance y :', variance(listeY))
print(' coeff de corrélation linéaire  :', coeff_correlation_lineaire(listeX, listeY))


plt.scatter(listeX, listeY)
plt.xlabel("épreuve A")
plt.ylabel("épreuve B")
plt.show()

